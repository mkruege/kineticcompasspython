# RESTRICTION TESTING FOR SIMULATED EXPERIMENTS FOR KINETIC COMPASS
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08

import numpy as np
import seaborn as sns
import pickle
import ensMeth
from matplotlib import pyplot as plt

def max_experiment_time(exp_pickle, gridsize, gridbounds, loggrid):
    [uncert_preds_params, uncert_preds] = pickle.load(open("sim_uncert_grid_km3.pickle", "rb"))
    [all_preds_params, all_preds] = pickle.load(open(exp_pickle[0], "rb"))  # load specified pickled grid
    [radgrid, Xgrid] = ensMeth.get_grid(gridsize, gridbounds, loggrid)  # load current grid
    all_preds = np.log10(all_preds)
    #conditions: np.max(pred_f) > 259200 or np.min(pred_f) < 1
    pemax = np.zeros((100, 100))
    peminmax = np.zeros((100,100))
    peavgmax = np.zeros((100, 100))
    lockgrid = np.zeros((100, 100))
    #peavgmin = np.zeros((100, 100))
    count = 0
    for i in range(100):
        for j in range(100):
            PE_max = np.max(all_preds[count])
            PE_avgmax = np.average([np.max(all_preds[count, k, :]) for k in range(all_preds[count].shape[0])])
            #PE_min = np.min(all_preds[count])
            PE_minmax = np.min([np.max(all_preds[count, k, :]) for k in range(all_preds[count].shape[0])])
            pemax[i, j] += PE_max
            peminmax[i, j] += PE_minmax
            peavgmax[i, j] += PE_avgmax
            #peavgmin[i, j] += PE_avgmin
            if PE_max > np.log10(259200) or PE_minmax < np.log10(1) or uncert_preds[count][0][0] == 0:
                lockgrid[i, j] = 1
            count = count+1
    fig, ax = plt.subplots(figsize=(8, 8))
    cmap = 'coolwarm'
    levels = 40
    contourf = ax.contourf(pemax, cmap=cmap, levels=levels)
    contour = ax.contour(pemax, levels=[np.log10(259200)], colors='k', linewidths=3)
    contour2 = ax.contour(pemax, levels=[np.log10(1)], colors='k', linewidths=3)
    cbar = fig.colorbar(contourf, ax=ax)
    cbar.ax.set_ylabel('Predicted Maximum Experiment Duration', rotation=270, labelpad=15)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_aspect('equal')
    plt.savefig("max_prediction_time.pdf")
    plt.show()

    fig, ax = plt.subplots(figsize=(8, 8))
    cmap = 'coolwarm'
    levels = 40
    contourf = ax.contourf(peavgmax, cmap=cmap, levels=levels)
    contour = ax.contour(peavgmax, levels=[np.log10(259200)], colors='k', linewidths=3)
    contour2 = ax.contour(peavgmax, levels=[np.log10(1)], colors='k', linewidths=3)
    cbar = fig.colorbar(contourf, ax=ax)
    cbar.ax.set_ylabel('Predicted Average Experiment Duration', rotation=270, labelpad=15)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_aspect('equal')
    plt.savefig("avg_prediction_time.pdf")
    plt.show()

    fig, ax = plt.subplots(figsize=(8, 8))
    cmap = 'coolwarm'
    levels = 40
    contourf = ax.contourf(peminmax, cmap=cmap, levels=levels)
    contour = ax.contour(peminmax, levels=[np.log10(259200)], colors='k', linewidths=3)
    contour2 = ax.contour(peminmax, levels=[np.log10(1)], colors='k', linewidths=3)
    cbar = fig.colorbar(contourf, ax=ax)
    cbar.ax.set_ylabel('Predicted Minimum Experiment Duration', rotation=270, labelpad=15)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_aspect('equal')
    plt.savefig("min_prediction_time.pdf")
    plt.show()


    #plt.contourf(lockgrid, cmap=cmap, levels=levels)
    plt.contour(lockgrid, levels=[0.5], colors='b', linewidths=5)
    #plt.colorbar(contourf, ax=ax)
    #ax.set_xticks([])
    #ax.set_yticks([])
    #ax.set_aspect('equal')
    plt.savefig("time_boundaries.pdf")
    plt.show()

    pickle.dump(lockgrid, open("lockgrid.pickle", "wb"))

    return

def calculate_average_max(row):
    return np.mean(np.log10(np.max(row, axis=1)))