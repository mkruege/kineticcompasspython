# EXAMPLE APPLICATIONS OF THE KINETIC COMPASS FRAMEWORK
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08


# read data and transform to pickle file
import mat_to_py
mat_to_py.transform_grid_mat_to_pickle("KM3_GridSampling_20220924", "fullgrid_km3") # fully evaluated grid (KM-SUB)


mat_to_py.transform_grid_mat_to_pickle("SAVE_GRID_20221110", "sim_uncert_grid_km3") # 500 KM-SUB Fits on experimentally possible environmental conditions grid. Simulated uncertainty of r_uncert = 0.05; x_uncert = 0.02; t_uncert = 0.07;


# experimental data
cmprdata_in = [[2.70E-03, 3.69E+14, 1.89E+21], [2.70E-03, 4.92E+12, 1.89E+21], [2.00E-05, 7E+13, 1.20E+21],
                   [4.00E-05, 2.50E+15, 1.89E+21], [2.50E-05, 2E+14, 1.89E+21], [2.50E-05, 3.25E+14, 1.89E+21],
                   [2.50E-05, 5.51E+14, 1.89E+21]]

cmprdata_out_d = [list(reversed([0.604, 0.721, 0.427, 0.558, 0.11, 0.144])),
                      list(reversed([0.676, 0.822, 0.732, 0.676, 0.188, 0.204, 0.0962, 0.106])),
                      list(reversed([0.975156, 0.938477, 0.8836, 0.855625, 0.81, 0.765625, 0.7921, 0.680625, 0.650039, 0.600625,
                       0.543906, 0.525625])),
                      list(reversed([0.91, 0.792, 0.724, 0.595, 0.522, 0.384, 0.291, 0.227])), list(reversed([0.8673, 0.6122, 0.4468, 0.1785])),
                      list(reversed([0.7858, 0.4942, 0.2664])), list(reversed([0.5091, 0.2745, 0.1077]))]

cmprdata_out_t = [list(reversed([240.108, 240.108, 398.374, 398.374, 899.729, 899.729])),
                      list(reversed([9000, 9000, 18000, 18000, 67500, 67500, 80500, 80500])),
                      list(reversed([1.3, 2.5, 4.1, 5.2, 6.38, 7.5, 9, 10.3, 11.5, 13.1, 14.3, 15.4])),
                      list(reversed([0.2313, 0.4579, 0.7202, 0.9292, 1.2205, 1.6982, 2.1938, 2.6898])),
                      list(reversed([3.3, 7.2, 14.7, 35.24])), list(reversed([3.06, 6.65, 14.57])), list(reversed([2.8, 6.75, 14.55]))]

cmprdata = [cmprdata_in, [cmprdata_out_d, cmprdata_out_t]]

# test and plot boundaries for experiment duration
import PE_stats
PE_stats.max_experiment_time(["sim_uncert_grid_km3.pickle"], gridsize=[100, 100], gridbounds=[[-6,-2],[10,16]], loggrid=1)
PE_stats.max_experiment_time(["fullgrid_km3.pickle"], gridsize=[100, 100], gridbounds=[[-6,-2],[10,16]], loggrid=1)

# fit ensemble acquisition for NN surrogate model
import sample_fits
sample_fits.test_loguniform_batch_sampling("final_surr_model.h5", "final_x_y_scaler.sav", 10000, 500, cmprdata_out_d, cmprdata_out_t, cmprdata_in, rRMSE_lim=0.0105)

# uncertainty calibration via error development
import Simulation_Iterator
uncert = [0.06, 0.07, 0.08, 0.09, 0.1]
env_uncert = [[0.05, 0.02], [0.05, 0.01], [0.05, 0.03], [0.04, 0.02], [0.06, 0.02], [0.04, 0.03], [0.06, 0.01]]
for unc in uncert:
    for env_unc in env_uncert:
        Simulation_Iterator.calibrate_error_th("final_surr_mod_fits_sorted_w_err_6select.csv", "final_surr_mod_fits_sorted_labeled_6select.csv", 6, 5, 20, "final_surr_model.h5", "final_x_y_scaler.sav", 6, cmprdata, gridsize=[100,100], gridbounds=[[-6,-2],[10,16]], loggrid=1, env_uncert=env_unc, add_uncert=unc)

# FULL SIMULATIONS FOR ENSEMBLE SPREAD:
# for simple ES evaluation (no simulation) set third argument (iter) to 1
# you can provide pre-evaluated ensemble spread matrices as 'useprevious', e.g. useprevious="km3_ensembe_spreads_matrix.csv"
#km-sub
Simulation_Iterator.iterate_fits("km3_fits_sorted_resampled.csv", "km3_fits_sorted_resampled_labeled.csv", 5, "km3", "not_relevant_for_km", 500, 500, "km_sub_sim", cmprdata, runfits=[], gridsize=[100,100], gridbounds=[[-6,-2],[10,16]], loggrid=1, ignore_ES_frame=1, add_uncert=0.07, env_uncert=[0.05, 0.02],  useprevious="", usepickle=["fullgrid_km3.pickle"], filter_th=0.0105, exp_pickle=["sim_uncert_grid_km3.pickle"], lockgrid=["lockgrid.pickle"])
#random
Simulation_Iterator.iterate_fits("km3_fits_sorted_resampled.csv", "km3_fits_sorted_resampled_labeled.csv", 5, "km3", "not_relevant_for_km", 500, 500, "km_sub_random_sim", cmprdata, runfits=[], set_random_experiment=1, gridsize=[100, 100], gridbounds=[[-6,-2],[10,16]], loggrid=1, ignore_ES_frame=1, add_uncert=0.07, env_uncert=[0.05, 0.02],  useprevious="", usepickle=["fullgrid_km3.pickle"], filter_th=0.0105, exp_pickle=["sim_uncert_grid_km3.pickle"], lockgrid=["lockgrid.pickle"])
#hybrid model 1: km3 fits and surr model evaluation
Simulation_Iterator.iterate_fits("km3_fits_sorted_resampled.csv", "km3_fits_sorted_resampled_labeled.csv", 5, "final_surr_model.h5", "final_x_y_scaler.sav", 500, 500, "hybrid_1_sim", cmprdata, runfits=[], gridsize=[100, 100], gridbounds=[[-6,-2],[10,16]], loggrid=1, ignore_ES_frame=1, add_uncert=0.07, env_uncert=[0.05, 0.02],  useprevious="", usepickle=["fullgrid_final_hybrid_model.pickle"], filter_th=0.0105, exp_pickle=["sim_uncert_grid_km3.pickle"], lockgrid=["lockgrid.pickle"])
#hybrid model 2: km3 fits and surr model evaluation, km3 fit filtering
Simulation_Iterator.iterate_fits("km3_fits_sorted_resampled.csv", "km3_fits_sorted_resampled_labeled.csv", 5, "final_surr_model.h5", "final_x_y_scaler.sav", 500, 500, "hybrid_2_sim", cmprdata, runfits=[], gridsize=[100, 100], gridbounds=[[-6,-2],[10,16]], loggrid=1, ignore_ES_frame=1, add_uncert=0.07, env_uncert=[0.05, 0.02],  useprevious="", usepickle=["fullgrid_final_hybrid_model.pickle"], filter_th=0.0105, exp_pickle=["sim_uncert_grid_km3.pickle"], specified_filter_pickle=["fullgrid_km3.pickle"], lockgrid=["lockgrid.pickle"])
#surr_model
Simulation_Iterator.iterate_fits("final_surr_mod_fits_sorted.csv", "final_surr_mod_fits_sorted_labeled.csv", 5, "final_surr_model.h5", "final_x_y_scaler.sav", 500, 500, "surr_mod_sim", cmprdata, runfits=[], gridsize=[100,100], gridbounds=[[-6,-2],[10,16]], loggrid=1, ignore_ES_frame=1, add_uncert=0.07, env_uncert=[0.05, 0.02],  useprevious="", usepickle=["fullgrid_surr_mod_final.pickle"], filter_th=0.0105, exp_pickle=["sim_uncert_grid_km3.pickle"], lockgrid=["lockgrid.pickle"])

# PARAMETER CONSTRAINT POTENTIAL
import Simulation
allparams = ["RS1", "RB1", "dXY", "dYY", "HXY", "deX", "acX"]
borderval=[7,9,6,6,3,7,4]
for i, param in enumerate(allparams):
    Simulation.perform_simulation("km3_fits_sorted_resampled.csv", "km3_fits_sorted_resampled_labeled.csv", 1, "km3", "not_relevant", 500, cmprdata, subcluster_meth=[param], continuous_density=20, continuous_bins=0.05, boundnorm=borderval[i], gridsize=[100, 100], ignore_ES_frame=1, add_uncert=0.07, env_uncert=[0.05, 0.02], select_gt_by_indx=[0], useprevious="", usepickle=["fullgrid_km3.pickle"], filter_th=0.0105, exp_pickle=["sim_uncert_grid_km3.pickle"], exp_distance=0.1, lockgrid=["lockgrid.pickle"])
    Simulation.perform_simulation("final_surr_mod_fits_sorted.csv", "final_surr_mod_fits_sorted_labeled.csv", 1, "final_surr_model.h5", "final_x_y_scaler.sav", 500, cmprdata, subcluster_meth=[param], continuous_density=20, continuous_bins=0.05, boundnorm=borderval[i], gridsize=[100, 100], ignore_ES_frame=1, add_uncert=0.07, env_uncert=[0.05, 0.02], select_gt_by_indx=[0], useprevious="", usepickle=["fullgrid_surr_mod_final.pickle"], filter_th=0.0105, exp_pickle=["sim_uncert_grid_km3.pickle"], exp_distance=0.1, lockgrid=["lockgrid.pickle"])

#####################Arguments for Simulation#################################
#subcluster_meth=[]         evaluates constraint potential instead of ensemble spread, specify parameter names
#subcluster_bin_size=0.5        fixed size of bins -> in log
#subcluster_error_bin=0.011     size of bins determined by error calculation - overrules bin_size
#continuous_density=10      for moving average, only one in x points is calculated
#continuous_bins=1          no bins, but continuous moving average - overrules error_bin
#gridsize=100               gridsize for Ensemble Method
#remove_ground_truth=0      if simulated truth remains in Ensemble Fits
#usecluster=-1              specifically select simulated truth from cluster 0 or 1
#ignore_ES_frame=0          don't place experiments too close to the border (steps on grid)
#add_uncert=0               add experimental uncertainty to simulated experiment (sigma for log10 normal distribution)
#env_uncert=[]              add experimental uncertainty for environmental parameters [rad, X0]
#set_random_experiment=0    don't use ensemble method for determination of experiments (random) -> benchmarking
#useprevious=""             skip first Ensemble Method run by providing output file
#exp_distance=0.2           minimim distance between simulated experiments (on a rad - X0 log10 plane)
#select_gt_by_indx=[]       add one index to list for a specific fit to use as simulated truth
#restrict_exp_cond=1        restrict outputs of simulated experiment (1s < exp_time < 3d)
#smooth_interp=1            smooth=1 is 2nd order spline interpolation, smooth=0 is 1st order spline interpolation to determine Ensemble Spread
#filter_by_gt_calibration=0   filters fits by maximal relative deviation (value) from the error experimental uncertainty (simulated exp with ground truth)
#km_exp=1                   simulates experiment by calling KM model in matlab
#error_th_calibration=0     different mode: checks error development of fits over multiple iterations to calibrate experimental uncertainty
#exp_iter=0                   relevant to loop error_th_calibration
#external_exp=[]               select experiment externally before calling the simulation
#makepickle="current.pickle"    select name to save full grid as pickle if no pickle is used
#usepickle=[]               load pickle to avoid calculating full grid
#exp_pickle=[]              load experiment from grid to avoid calling a model -> only works without env_uncertainty
#filter_th=1.3              remove fits from Ensemble above this error threshold (outdated)
#relative_fit_filtering=0   filters fits not by absolute threshold, but relative to best fit (outdated)
#filter_by_exchange=0       filters fits by leave-one-out-principle with specified fraction of fits better than original error (outdated)
#lockgrid=[]                grid file indicating which experimental conditions are valid
#specified_filter_pickle=[] grid file to filter fits from different gridfile as constraint potential evaluation gridfile (for comparable hybrid simulation)
######################################################################################################################