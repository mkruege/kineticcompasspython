Original python code for the development and testing of the Kinetic Compass  
Matteo Krüger (m.krueger@mpic.de)  
Multiphase Chemistry Department  
Max Planck Institute for Chemistry, Mainz  
Last update: 2023/08/08  
  
For an application of the Kinetic Compass, the 'clean' Julia implementation is highly recommended!  
https://gitlab.mpcdf.mpg.de/mkruege/kineticcompass  
  
  
Contents:  
  
'main.py' 			examples for program runs  
'ensMeth.py'			Kinetic Compass core functions  
'Simulation.py' 		core functions for simulations  
'Simulation_Iterator.py'	wrapper function to run a multitude of simulations  
'mat_to_py' 			transformation of MATLAB data to Python data files (pickle or csv)  
'error_th_calibration.py'	uncertainty calibration method for simulated experiments  
'plotting.py'			plotting functions  
'PE_stats.py'			functions to analyse pre-evaluated grids (restrictions and boundaries)  
'sample_fits.py'		functions to sample fit ensembles  
'scan_fits.py'			re-evaluation of fit ensembles (new threshold or model)  
'errorMeth.py'			alternative approach to test sensitivity of experimental parameters under various conditions  
'Clustering.py'			functions to cluster fit ensembles and add labels  
'mogon_exec.py'			high performance computing  
'default_config.txt'		example config file for high performance computing  
