# CLUSTERING FOR FIT ENSEMBLES
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08


import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans
import seaborn as sns
from sklearn.cluster import SpectralClustering
from sklearn.manifold import TSNE
from sklearn.cluster import AgglomerativeClustering


# load data - replace a one-line command by a three-line function that will be called ;)
def get_data(fit_file):
    df = pd.read_csv(fit_file)
    return df

def perform_KMeans_clustering(fit_file, n_clusters, algorithm, elbow=0):
    "Applies clustering on fit data provided in file. Algorithm: 0 - KMeans, 1 - Spectral Clustering, 2 - AggloClus, 3 - t-SNE + KMeans, 4 - t-SNE + AggloClus."

    fits = get_data(fit_file)
    fits = fits.dropna()
    fits = fits.reset_index(drop=True)
    try:
        fits = fits.drop(["rRMSE"], axis=1)
    except:
        pass
    try:
        fits_params = fits.drop(["out1", "out2", "out3"], axis=1)
    except:
        fits_params = fits
    log_fits_params = np.log10(fits_params)

    if elbow == 1:
        wcss = []
        for i in range(1, 11):
            kmeans = KMeans(n_clusters=i, init='k-means++', max_iter=300, n_init=10, random_state=0)
            kmeans.fit(fits_params)
            wcss.append(kmeans.inertia_)
        plt.plot(range(1, 11), wcss)
        plt.title('Elbow Method')
        plt.xlabel('Number of clusters')
        plt.ylabel('WCSS')
        plt.show()

    # call clustering algorithm
    if algorithm == 0:
        cluster = KMeans(n_clusters=n_clusters, random_state=0)
        alg = "KMeans"
    elif algorithm == 1:
        cluster = SpectralClustering(n_clusters=n_clusters)
        alg = "SpectralClust"
    elif algorithm == 2:
        cluster = AgglomerativeClustering(n_clusters=n_clusters)
        alg = "AggloClust"
    elif algorithm == 3:
        cluster = KMeans(n_clusters=n_clusters, random_state=0)
        tsne = TSNE(n_components=n_clusters)
        alg = "t-SNE_KMeans"
    elif algorithm == 4:
        cluster = AgglomerativeClustering(n_clusters=n_clusters)
        tsne = TSNE(n_components=n_clusters)
        alg = "t-SNE_AggloClust"
    else:
        print("No algorithm selected - use 0 - 4 as argument.")
        return


    # applies clustering algorithm and saves labels in dataframe
    cluster.fit(log_fits_params)
    log_fits_params["label"] = cluster.labels_

    log_fits_params.to_csv("labeled_curr_all.csv")

    # makes scatter matrix with labels from clustering algorithm
    sns.pairplot(log_fits_params, hue="label")
    plt.savefig("PlotMatrix_" + alg + ".pdf")
    plt.show()

    # makes dimensionality reduction scatter for individual algorithms
    if algorithm == 0:
        z = cluster.fit_transform(log_fits_params)
        df = pd.DataFrame()
        df["y"] = cluster.labels_
        df["comp-1"] = z[:, 0]
        df["comp-2"] = z[:, 1]
        sns.scatterplot(x="comp-1", y="comp-2", hue=df.y.tolist(), s=10,
                        palette=sns.color_palette("hls", n_clusters),
                        data=df).set(title=alg)
        plt.savefig("DimRed_" + alg + ".svg")
        plt.show()

    # makes dimensionality reduction with t-SNE and uses other algorithm (KMeans or AgglClust) for labeling
    if algorithm  == 3 or algorithm == 4:
        z = tsne.fit_transform(log_fits_params)
        df = pd.DataFrame()
        df["y"] = cluster.labels_
        df["comp-1"] = z[:, 0]
        df["comp-2"] = z[:, 1]

        sns.scatterplot(x="comp-1", y="comp-2", hue=df.y.tolist(), s=10,
                        palette=sns.color_palette("hls", n_clusters),
                        data=df)
        plt.savefig("DimRed_" + alg + ".svg")
        plt.show()

    return