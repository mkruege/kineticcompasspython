CONFIG FILE FOR ENSEMBLE METHOD ENSEMBLE
Lines starting with + will be executed in python and set the parameters for the program. Do not add anything to the + lines. A short description is provided for each parameter.



+ens_fits="final_surr_mod_fits_sorted.csv"
+labeled_ens_fits="final_surr_mod_fits_sorted_labeled.csv"
+iters=1
+model="final_surr_model.h5"
+scaler="final_x_y_scaler.sav"
+n_FE=500
+runs=2
+runname="test"
+runfits=[]
evaluates constraint potential instead of ensemble spread, specify parameter names
+subcluster_meth=[]         
fixed size of bins -> in log
+subcluster_bin_size=0.5     
size of bins determined by error calculation - overrules bin_size   
+subcluster_error_bin=0.011     
for moving average, only one in x points is calculated
+continuous_density=10
no bins, but continuous moving average - overrules error_bin
+continuous_bins=1
gridsize for Ensemble Method
+gridsize=100
if simulated truth remains in Ensemble Fits
+remove_ground_truth=0
specifically select simulated truth from cluster 0 or 1
+usecluster=-1
don't place experiments too close to the border (steps on grid)
+ignore_ES_frame=0
add experimental uncertainty to simulated experiment (sigma for log10 normal distribution)
+add_uncert=0
add experimental uncertainty for environmental parameters [rad, X0]
+env_uncert=[]
don't use ensemble method for determination of experiments (random) -> benchmarking
+set_random_experiment=0
skip first Ensemble Method run by providing output file
+useprevious="final_surr_mod_ensembe_spreads_matrix.csv"
minimim distance between simulated experiments (on a rad - X0 log10 plane)
+exp_distance=0.2
add one index to list for a specific fit to use as simulated truth
+select_gt_by_indx=[]
restrict outputs of simulated experiment (1s < exp_time < 3d)
+restrict_exp_cond=1
smooth=1 is 2nd order spline interpolation, smooth=0 is 1st order spline interpolation to determine Ensemble Spread
+smooth_interp=1
filters fits by maximal relative deviation (value) from the error experimental uncertainty (simulated exp with ground truth)
+filter_by_gt_calibration=0
simulates experiment by calling KM model in matlab
+km_exp=1
different mode: checks error development of fits over multiple iterations to calibrate experimental uncertainty
+error_th_calibration=0
relevant to loop error_th_calibration
+exp_iter=0
select experiment externally before calling the simulation
+external_exp=[]
select name to save full grid as pickle if no pickle is used
+makepickle=""
load pickle to avoid calculating full grid
+usepickle=["fullgrid_surr_mod_final.pickle"]
load experiment from grid to avoid calling a model -> only works without env_uncertainty
+exp_pickle=[]
remove fits from Ensemble above this error threshold (outdated)
+filter_th=0.013
filters fits not by absolute threshold, but relative to best fit (outdated)
+relative_fit_filtering=0
filters fits by leave-one-out-principle with specified fraction of fits better than original error (outdated)
+filter_by_exchange=0