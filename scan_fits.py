# RE-EVALUATION OF FIT ENSEMBLES
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08

from tensorflow import keras
import pandas as pd
import Matlab_Sampling
import pickle
import numpy as np
from scipy import io

import Simulation


def transform_mat_to_csv(filename):
    if filename.endswith(".mat"):
        # print(os.path.join(directory, filename))
        mat = io.loadmat(filename)
        df = pd.DataFrame(data=mat["OUT_GOOD"]).iloc[:, :7]

    #all_data = all_data.sort_values([24], axis=0)
    header = ["RS1", "RB1", "dXY", "dYY", "HXY", "deX", "acX"]
    df.to_csv("final_km_sub_fits_for_pairplot.csv", header=header, index=False)

    return


def scan_csv_with_new_th(input_file, rRMSEth, cmprdata, model, scaler, name="test_"):

    if not "KM3" in model:
        # LOAD NN FORWARD MODEL (3 outputs)
        NNmodel = keras.models.load_model(model)

        # LOAD x- and y-scaler associated to this model
        [scaler_x, scaler_y] = pickle.load(open(scaler, "rb"))

    fits = pd.read_csv(input_file)
    fits = fits.dropna()
    fits = fits.reset_index(drop=True)
    if "MSLE" in fits.columns:
        filtered_fits = fits[fits['MSLE'] < rRMSEth]
    else:
        try:
            only_fits = fits.drop(["out1", "out2", "out3", "out4", "out5", "out6", "out7", "out8", "out9"], axis=1)
        except:
            try:
                only_fits = fits.drop(["rRMSE"], axis=1)
            except:
                only_fits = fits
        rRMSEs = []
        if not "KM3" in model:
            for rown, row in enumerate(only_fits.iterrows()):
                print("Step " + str(rown) + " of " + str(only_fits.shape[0]))
                x = list(row[1])
                preds = nn_model_pred([np.transpose(np.append(x, cmprdata[0][0])), np.transpose(np.append(x, cmprdata[0][1])), np.transpose(np.append(x, cmprdata[0][2])), np.transpose(np.append(x, cmprdata[0][3])), np.transpose(np.append(x, cmprdata[0][4])), np.transpose(np.append(x, cmprdata[0][5])), np.transpose(np.append(x, cmprdata[0][6]))], NNmodel, scaler_x, scaler_y)
                #else:
                #    preds = []
                #    for i in range(7):
                #        preds.append(Matlab_Sampling.get_KM3_sample([np.transpose(np.append(x, cmprdata[0][0])), np.transpose(np.append(x, cmprdata[0][1])), np.transpose(np.append(x, cmprdata[0][2])), np.transpose(np.append(x, cmprdata[0][3])), np.transpose(np.append(x, cmprdata[0][4])), np.transpose(np.append(x, cmprdata[0][5])), np.transpose(np.append(x, cmprdata[0][6]))][i].tolist(), sim=0))
                #curr_err = calc_error_6sets(preds, data_out)
                curr_err = Simulation.calc_error_Xsets([], preds, [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1], cmprdata=cmprdata)
                print(curr_err)
                rRMSEs.append(curr_err)
        else:
            [rRMSEs, times, dumpfits] = Matlab_Sampling.get_KM3_multisample_updated(only_fits, 500, cmprdata, rRMSEth, returnouts=0)
        print(rRMSEs)
        fits["MSLE"] = rRMSEs
        filtered_fits = fits[fits["MSLE"] < rRMSEth]
        #filtered_fits = filtered_fits[filtered_fits["acX"] < 1]

    filtered_fits.to_csv(name + "filtered_fit_ensemble.csv")
    return


def nn_model_pred(inpts, NNmodel, scaler_x, scaler_y, log=0):

    inpts_log = np.log10(inpts)
    inpts_t = scaler_x.transform(inpts_log)
    pred = NNmodel.predict(inpts_t)
    pred_t = scaler_y.inverse_transform(pred)
    pred_f = 10**pred_t

    if log==0:
        return pred_f
    else:
        return pred_t

#OUTDATED
def calc_error_6sets(preds, data, logabs=1):
    "Calculates relative root mean square error in comparison with 6 data sets"

    d = []
    for row in data:
        dr = list(row)
        d.append(dr)

    preds_l = []
    for row in preds:
        try:
            preds_l.append(row.tolist()[0])
        except:
            preds_l.append(row[0])
    leaveout = []
    for n, dat in enumerate(d):
        while "no_data" in dat:
            leaveout.append([n, dat.index("no_data")])
            del preds_l[n][dat.index("no_data")]
            del d[n][dat.index("no_data")]

    if logabs == 1:
        J = []
        for i, idat in enumerate(d):
            N = []
            for j, jdat in enumerate(idat):
                N.append((np.log10(preds_l[i][j]) - np.log10(d[i][j])) ** 2)
            J.append(np.average(N))
        rRMSE = np.average(J)

    else:
        J = []
        for i, idat in enumerate(d):
            N = []
            for j, jdat in enumerate(idat):
                N.append((((preds_l[i][j] - d[i][j])/d[i][j])**2)/len(d[i]))
            J.append(np.sqrt(np.sum(N)))
        rRMSE = np.sum(J)

    return rRMSE