# HIGH PERFORMANCE COMPUTING FOR KINETIC COMPASS SIMULATIONS
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08


from sys import argv
import argparse
import Simulation_Iterator
import Simulation


model_dir = './'
if argv[0].find('/') >= 0:
    model_dir = argv[0][: - argv[0][::-1].find('/')]


#parse arguments
parser = argparse.ArgumentParser(description='Launching iteration over HPT sheet for specific training set size.')
parser.add_argument('--config_file', '-cf', type=str, default='test_config.txt', help='Path to bed file with configurations')



# parse and pre-process command line arguments
args = parser.parse_args()

print("Arguments parsed")

cmprdata_in = [[2.70E-03, 3.69E+14, 1.89E+21], [2.70E-03, 4.92E+12, 1.89E+21], [2.00E-05, 7E+13, 1.20E+21],
                   [4.00E-05, 2.50E+15, 1.89E+21], [2.50E-05, 2E+14, 1.89E+21], [2.50E-05, 3.25E+14, 1.89E+21],
                   [2.50E-05, 5.51E+14, 1.89E+21]]

cmprdata_out_d = [list(reversed([0.604, 0.721, 0.427, 0.558, 0.11, 0.144])),
                      list(reversed([0.676, 0.822, 0.732, 0.676, 0.188, 0.204, 0.0962, 0.106])),
                      list(reversed([0.975156, 0.938477, 0.8836, 0.855625, 0.81, 0.765625, 0.7921, 0.680625, 0.650039, 0.600625,
                       0.543906, 0.525625])),
                      list(reversed([0.91, 0.792, 0.724, 0.595, 0.522, 0.384, 0.291, 0.227])), list(reversed([0.8673, 0.6122, 0.4468, 0.1785])),
                      list(reversed([0.7858, 0.4942, 0.2664])), list(reversed([0.5091, 0.2745, 0.1077]))]

cmprdata_out_t = [list(reversed([240.108, 240.108, 398.374, 398.374, 899.729, 899.729])),
                      list(reversed([9000, 9000, 18000, 18000, 67500, 67500, 80500, 80500])),
                      list(reversed([1.3, 2.5, 4.1, 5.2, 6.38, 7.5, 9, 10.3, 11.5, 13.1, 14.3, 15.4])),
                      list(reversed([0.2313, 0.4579, 0.7202, 0.9292, 1.2205, 1.6982, 2.1938, 2.6898])),
                      list(reversed([3.3, 7.2, 14.7, 35.24])), list(reversed([3.06, 6.65, 14.57])), list(reversed([2.8, 6.75, 14.55]))]

cmprdata = [cmprdata_in, [cmprdata_out_d, cmprdata_out_t]]

with open(args.config_file) as configfile:
    lines = configfile.readlines()
    for line in lines:
        if len(line) != 0:
            if line[0] == "+":
                exec(line[1:])


Simulation_Iterator.iterate_fits_parallel(ens_fits, labeled_ens_fits, iters, model, scaler, n_FE, runs, runname, cmprdata, subcluster_meth=subcluster_meth,  subcluster_bin_size=subcluster_bin_size, subcluster_error_bin=subcluster_error_bin, continuous_density=continuous_density, continuous_bins=continuous_bins, runfits=runfits, gridsize=gridsize, remove_ground_truth=remove_ground_truth, usecluster=usecluster, ignore_ES_frame=ignore_ES_frame, restrict_exp_cond=restrict_exp_cond, add_uncert=add_uncert, env_uncert=env_uncert, filter_th=filter_th, set_random_experiment=set_random_experiment, useprevious=useprevious, exp_distance=exp_distance, smooth_interp=smooth_interp, relative_fit_filtering=relative_fit_filtering, filter_by_exchange=filter_by_exchange, filter_by_gt_calibration=filter_by_gt_calibration, km_exp=km_exp, usepickle=usepickle, makepickle=makepickle, exp_pickle=exp_pickle)


