# FIT ENSEMBLE ACQUISITION FOR SURROGATE MODELS
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08

import time
import ensMeth
from tensorflow import keras
import numpy as np
import seaborn as sns
import pickle
import random
import pandas as pd
import csv
import scipy
import matplotlib.pyplot as plt

def compare_fit_ensembles(fe1_file, fe2_file, model2_file, model2_scaler, cmprdata_in, cmprdata_out, prevdata=[]):
    if len(prevdata) == 0:

        import scan_fits
        # scan model 1 fe with model 2
        scan_fits.scan_csv_with_new_th(fe1_file, 100, [cmprdata_in, cmprdata_out], model2_file, model2_scaler, name="fe1_mod2_")
        # scan model 2 fe with model 1
        scan_fits.scan_csv_with_new_th(fe2_file, 100, [cmprdata_in, cmprdata_out], "KM3", "anyscaler", name="fe2_mod1_")


    else:
        kmfe = pd.read_csv(prevdata[0])
        nnfe = pd.read_csv(prevdata[1])

        kmfe_kmerr = kmfe["KMerr"].tolist()
        kmfe_nnerr = kmfe["NNerr"].tolist()
        nnfe_kmerr = nnfe["KMerr"].tolist()
        nnfe_nnerr = nnfe["NNerr"].tolist()


        plt.scatter(kmfe_kmerr, kmfe_nnerr, label="KM-SUB-FE", s=0.5)
        plt.scatter(nnfe_kmerr, nnfe_nnerr, label="NN-SM-FE", s=0.5)
        plt.legend(fontsize=12)
        plt.xlabel("KM-SUB evaluated MSLE", fontsize=12)
        plt.ylabel("NN-SM evaluated MSLE", fontsize=12)
        plt.xlim([0.0075, 0.015])
        plt.ylim([0.0075, 0.015])
        plt.savefig("error_matrix_FE.pdf")
        plt.show()

        plt.scatter(kmfe_kmerr, kmfe_nnerr, label="KM-SUB-FE", s=0.7)
        plt.scatter(nnfe_nnerr, nnfe_kmerr, label="NN-SM-FE", s=0.7)
        plt.legend()
        plt.xlabel("Original MSLE")
        plt.ylabel("Cross-evaluated MSLE")
        plt.xlim([0.0075, 0.015])
        plt.ylim([0.0075, 0.015])
        plt.show()

        crctkmfe = 0
        crctnnfe = 0
        falsekmfe = 0
        falsennfe = 0
        for i in range(len(kmfe_kmerr)):
            if kmfe_nnerr[i] < 0.0105:
                crctkmfe = crctkmfe + 1
            else:
                falsekmfe = falsekmfe + 1
            if nnfe_kmerr[i] < 0.0105:
                crctnnfe = crctnnfe + 1
            else:
                falsennfe = falsennfe + 1

        print("KM-SUB-FE: " + str(crctkmfe) + ", " + str(falsekmfe))
        print("NN-SM-FE: " + str(crctnnfe) + ", " + str(falsennfe))

    return

def test_loguniform_batch_sampling(model, scaler, batchsize, desiredfits, cmprdata_out_d, cmprdata_out_t, cmprdata_in, rRMSE_lim=2, verbose=1):
    "Function to call loguniform batch sampling with the NN surrogate model, time and print the full results."

    time1 = time.time()
    [testedsamples, nfits] = perform_loguniform_sampling(model, scaler, desiredfits, batchsize, cmprdata_out=[cmprdata_out_d, cmprdata_out_t], cmprdata_in=cmprdata_in, rRMSE_lim=rRMSE_lim, verbose=verbose)
    time2 = time.time()
    sampletime = time2 - time1
    print("#############NN LOGUNIFORM sampling###############")
    print("rRMSE threshold: " + str(rRMSE_lim))
    print("Tested samples: " + str(testedsamples))
    print("Time required [s]: " + str(sampletime))
    print("Fits: " + str(nfits))
    print("Acceptance rate: " + str((nfits/testedsamples) * 100) + " %")
    print("######################################")
    return

def NN_suggested_KM_SUB_sampling(filename, desiredruns, cmprdata_out, rRMSE_lim=5):
    "Function to re-sample e.g. NN-suggested Fits in order to find KM-SUB fits among them."

    import Matlab_Sampling

    fits = pd.read_csv(filename)
    try:
        fits_params = fits.drop(["rRMSE"], axis=1)
    except:
        try:
            fits_params = fits.drop(["out1", "out2", "out3"], axis=1)
        except:
            fits_params = fits
    time1 = time.time()
    [errors, times, fits] = Matlab_Sampling.get_KM3_multisample(fits_params, desiredruns, cmprdata_out, rRMSE_lim)
    time2 = time.time()
    print("#############NN-suggested KM sampling###############")
    print("rRMSE threshold: " + str(rRMSE_lim))
    print("Tested samples: " + str(len(errors)))
    print("Time required [s]: " + str(time2 - time1))
    print("Fits: " + str(fits))
    #print("Acceptance rate: " + str((fits / len(errors)) * 100) + " %")
    print("################################################")
    print(errors)
    fits_params["rRMSE"] = errors
    fits_params.to_csv("nn_sugg_fit_ensemble.csv")
    return


def perform_loguniform_sampling(model, scaler, n_fits, it_samples, rRMSE_lim, cmprdata_out, cmprdata_in, outputs=9, verbose=0):
    "Function for broad batch sampling in loguniform space using the NN surrogate model. Hard-coded for KM-SUB with 9 outputs."

    testedsamples = 0

    # LOAD NN FORWARD MODEL (3 outputs)
    NNmodel = keras.models.load_model(model)

    # LOAD x- and y-scaler associated to this model
    [scaler_x, scaler_y] = pickle.load(open(scaler, "rb"))


    goodfits = []

    while len(goodfits) < n_fits:
        testedsamples=testedsamples+it_samples
        samples = it_samples
        if verbose == 1:
            print("Testing " + str(samples))
            print("Found " + str(len(goodfits)))

        kslr = lognuniform(-15, -8, samples, 10)
        kbr = lognuniform(-20, -11, samples, 10)
        Dx = lognuniform(-11, -5, samples, 10)
        Dy = lognuniform(-12, -6, samples, 10)
        H_cp = lognuniform(-5.3, -2.3, samples, 10)
        Td = lognuniform(-9, -2, samples, 10)
        a0 = lognuniform(-4, 0, samples, 10)
        #rout = [0.0000325 for x in range(samples)]
        #Xg = [2.76*10**15 for x in range(samples)]
        #iniconc = [1.897*10**21 for x in range(samples)]

        df1 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[0][0] for x in range(it_samples)], [cmprdata_in[0][1] for x in range(it_samples)], [cmprdata_in[0][2] for x in range(it_samples)]]).transpose())
        df2 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[1][0] for x in range(it_samples)], [cmprdata_in[1][1] for x in range(it_samples)], [cmprdata_in[1][2] for x in range(it_samples)]]).transpose())
        df3 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[2][0] for x in range(it_samples)], [cmprdata_in[2][1] for x in range(it_samples)], [cmprdata_in[2][2] for x in range(it_samples)]]).transpose())
        df4 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[3][0] for x in range(it_samples)], [cmprdata_in[3][1] for x in range(it_samples)], [cmprdata_in[3][2] for x in range(it_samples)]]).transpose())
        df5 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[4][0] for x in range(it_samples)], [cmprdata_in[4][1] for x in range(it_samples)], [cmprdata_in[4][2] for x in range(it_samples)]]).transpose())
        df6 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[5][0] for x in range(it_samples)], [cmprdata_in[5][1] for x in range(it_samples)], [cmprdata_in[5][2] for x in range(it_samples)]]).transpose())
        df7 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[6][0] for x in range(it_samples)], [cmprdata_in[6][1] for x in range(it_samples)], [cmprdata_in[6][2] for x in range(it_samples)]]).transpose())

        #params1 = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [0.001 for x in range(samples)], [10000000000000 for x in range(samples)], [1.89E+21 for x in range(samples)]])
        #params2 = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [0.00002 for x in range(samples)], [69500000000000 for x in range(samples)], [1.89E+21 for x in range(samples)]])
        #params3 = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [0.0000325 for x in range(samples)], [2500000000000000 for x in range(samples)], [1.89E+21 for x in range(samples)]])
        #params4 = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [2.50E-05 for x in range(samples)], [2.00E+14 for x in range(samples)], [1.89E+21 for x in range(samples)]])
        #params5 = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [2.50E-05 for x in range(samples)], [3.25E+14 for x in range(samples)], [1.89E+21 for x in range(samples)]])
        #params6 = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [2.50E-05 for x in range(samples)], [5.51E+14 for x in range(samples)], [1.89E+21 for x in range(samples)]])
        x_new = pd.concat((df1, df2, df3, df4, df5, df6, df7), axis=0, ignore_index=1)
        y_new = nn_model_pred(x_new, NNmodel, scaler_x, scaler_y)
        rRMSEs = []
        for i in range(samples):
            #try:
            rRMSEs.append(calc_error_Xsets(
                    [y_new[i], y_new[i + it_samples], y_new[i + it_samples * 2], y_new[i + it_samples * 3],
                     y_new[i + it_samples * 4], y_new[i + it_samples * 5], y_new[i + it_samples * 6]], cmprdata_out))
            #except:
            #    rRMSEs.append(calc_error_6sets(
            #        [y_new[i], y_new[i + it_samples], y_new[i + it_samples * 2], y_new[i + it_samples * 3],
            #         y_new[i + it_samples * 4], y_new[i + it_samples * 5]], cmprdata_out, outputs))

            #preds = nn_model_pred([np.transpose(params1)[i], np.transpose(params2)[i], np.transpose(params3)[i], np.transpose(params4)[i], np.transpose(params5)[i], np.transpose(params6)[i]], NNmodel, scaler_x, scaler_y)
            #rRMSEs.append(calc_error_6sets(preds, cmprdata_out))

        #hist for NN prediction errors
        #bins = 10 ** (np.arange(-2.2, 2, 0.1))
        #plt.hist(np.log10(rRMSEs), bins=bins)
        #plt.xscale("log")
        #plt.plot()

        for n, rRMSE in enumerate(rRMSEs):
            if rRMSE < rRMSE_lim:
                #row = list(np.transpose(x_new)[n][:-3])
                row = list(x_new.iloc[n, :])[:-3]
                row.append(rRMSE)
                goodfits.append(row)

    header = ["RS1", "RB1", "dXY", "dYY", "HXY", "deX", "acX", "rRMSE"]
    with open("loguniform_fits.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(header)

    for row in goodfits:
        with open("loguniform_fits.csv", "a") as csv_file:  # add row to file
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)


    df = pd.read_csv("loguniform_fits.csv")
    logdf = np.log10(df)
    g = sns.pairplot(logdf)
    plt.show()

    return [testedsamples, len(goodfits)]


def lognuniform(low=0, high=1, size=None, base=np.e):
    return np.power(base, np.random.uniform(low, high, size))


def calc_error_6sets(preds, data, logabs=1):
    "Calculates relative root mean square error in comparison with 6 data sets"

    d = []
    for row in data:
        dr = list(row)
        d.append(dr)

    preds_l = []
    for row in preds:
        try:
            preds_l.append(row.tolist())
        except:
            preds_l.append(row)
    leaveout = []
    for n, dat in enumerate(d):
        while "no_data" in dat:
            leaveout.append([n, dat.index("no_data")])
            del preds_l[n][dat.index("no_data")]
            del d[n][dat.index("no_data")]

    if logabs == 1:
        J = []
        for i, idat in enumerate(d):
            N = []
            for j, jdat in enumerate(idat):
                N.append((np.log10(preds_l[i][j]) - np.log10(d[i][j])) ** 2)
            J.append(np.average(N))
        rRMSE = np.average(J)

    else:
        J = []
        for i, idat in enumerate(d):
            N = []
            for j, jdat in enumerate(idat):
                N.append((((preds_l[i][j] - d[i][j])/d[i][j])**2)/len(d[i]))
            J.append(np.sqrt(np.sum(N)))
        rRMSE = np.sum(J)

    return rRMSE

def calc_error_Xsets(preds, indata, logabs=1, inv=0):
    "Calculates relative root mean square error in comparison with 6 data sets"

    cmprdata_out_d = indata[0]

    cmprdata_out_t = indata[1]

    interpol_preds = []
    interpol_x = []
    if inv == 1:
        for pred in preds:
            xnew, ynew = ensMeth.interpolate(pred, 0.001, inv=1)
            interpol_preds.append(ynew)
            interpol_x.append(xnew)
    else:
        for pred in preds:
            interpol_preds.append(ensMeth.interpolate(pred, 0.001))

    # save interpolations for comparison/validation
    #import csv
    #with open('km3_outs_interp.csv', 'w', newline='') as file:
    #    writer = csv.writer(file)
    #    for row in interpol_preds:
    #        writer.writerow(row)

    d = cmprdata_out_t

    preds_l = []
    for rnum, row in enumerate(interpol_preds):
        curr = []
        if inv == 0:
            for cd in cmprdata_out_d[rnum]:
                curr.append(round(cd * 1000))
        else:
            for ic, cd in enumerate(cmprdata_out_t[rnum]):
                pass #not needed, not implemented (NN predictions would have to be extrapolated)
        preds_l.append([row[x] for x in curr])

    # leaveout = []
    # for n, dat in enumerate(d):
    #    while "no_data" in dat:
    #        leaveout.append([n, dat.index("no_data")])
    #        del preds_l[n][dat.index("no_data")]
    #        del d[n][dat.index("no_data")]

    if logabs == 1:
        J = []
        for i, idat in enumerate(d):
            N = []
            for j, jdat in enumerate(idat):
                N.append((np.log10(preds_l[i][j]) - np.log10(d[i][j])) ** 2)
            J.append(np.average(N))
        rRMSE = np.average(J)

    else:
        J = []
        for i, idat in enumerate(d):
            N = []
            for j, jdat in enumerate(idat):
                N.append((((preds_l[i][j] - d[i][j]) / d[i][j]) ** 2) / len(d[i]))
            J.append(np.sqrt(np.sum(N)))
        rRMSE = np.sum(J)

    return rRMSE

def nn_model_pred(inpts, NNmodel, scaler_x, scaler_y, log=0):
    "Simple helper function to obtain NN model predictions for given inputs."
    inpts_log = np.log10(inpts)
    inpts_t = scaler_x.transform(inpts_log)
    pred = NNmodel.predict(inpts_t)
    pred_t = scaler_y.inverse_transform(pred)
    pred_f = 10**pred_t

    if log==0:
        return pred_f
    else:
        return pred_t