# CALIBRATION OF EXPERIMENTAL UNCERTAINTY FOR KINETIC COMPASS SIMULATIONS
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08

import copy
import Simulation
import numpy as np
import ensMeth
from tensorflow import keras
import pickle

def perform_error_calibration(exp_iter, gt_i, gt_params, model, scaler, add_uncert, restrict_exp_cond, ignore_ES_frame, exp_distance, rad_X, km_exp, env_uncert, curr_i, cmprdata, gridsize, gridbounds, loggrid, lockgrid):
    "Function to perform uncertainty calibration as described in Simulation_Iterator.calibrate_error_th instead of usual Ensemble Method (called from Simulation)."

    # LOAD NN MODEL
    NNmodel = keras.models.load_model(model)

    # LOAD x- and y-scaler associated to this model
    [scaler_x, scaler_y] = pickle.load(open(scaler, "rb"))

    # data for previous experiments
    cmprdata_in = copy.deepcopy(cmprdata[0])


    if curr_i == 0:
        # loop over all fit parameters, append data for different experiments (simulated and real), calculate new rRMSE
        fits_params = [gt_params.to_numpy()]
        preds_exps = []
        for fit_param in fits_params:
            exp_params = []
            for trip in cmprdata_in:
                exp_params.append(np.append(fit_param, trip))

            # usual data transformation
            inpts_log = np.log10(exp_params)
            inpts_t = scaler_x.transform(inpts_log)
            pred = NNmodel.predict(inpts_t)
            pred_t = scaler_y.inverse_transform(pred)
            pred_f = 10 ** pred_t

            preds_exps.append(pred_f)

            errors = Simulation.calc_error_Xsets([], pred_f, cmprdata=cmprdata)

    else:
        # get individual values for multi-simulated experiments considering uncertainty (outs)
        outs = []
        for i in range(exp_iter):
            outs.append(Simulation.simulate_experiment(gt_i, gt_params, rad_X[-1][0], rad_X[-1][1], model, scaler, add_uncert,
                                           restrict_exp_cond, ignore_ES_frame, 1, exp_distance, rad_X,
                                           "", "", km_exp, env_uncert, [], gridsize, gridbounds, loggrid, lockgrid))

        # simulated experiments
        cmprdata_in = [rad_X[-1] + [1.89E+21]]


        errors = []
        # we will get one error for each multi-simulated experiment
        for out in outs:

            # loop over all fit parameters, append data for different experiments (simulated and real), calculate new rRMSE
            fits_params = [gt_params.to_numpy()]
            preds_exps = []
            for fit_param in fits_params:
                exp_params = []
                for trip in cmprdata_in:
                    exp_params.append(np.append(fit_param, trip))

                # usual data transformation
                inpts_log = np.log10(exp_params)
                inpts_t = scaler_x.transform(inpts_log)
                pred = NNmodel.predict(inpts_t)
                pred_t = scaler_y.inverse_transform(pred)
                pred_f = 10 ** pred_t

                preds_exps.append(pred_f)

                errors.append(calc_partial_error_Xsets(out[0], pred_f))


    return errors


def calc_partial_error_Xsets(sim_exp, preds, logabs=1):
    "A partial error only for the simulated experiments (must be included in original error to obtain comparable value)."


    data = []
    for exp in sim_exp:
        data.append(exp.tolist())


    d = []
    for row in data:
        dr = list(row)
        d.append(dr)

    preds_l = []
    for row in preds:
        preds_l.append(row.tolist())

    if logabs == 1:
        J = []
        for i, idat in enumerate(d):
            N = []
            for j, jdat in enumerate(idat):
                N.append((np.log10(preds_l[i][j]) - np.log10(d[i][j])) ** 2)
            J.append(np.average(N))
        rRMSE = np.average(J)

    else:
        J = []
        for i, idat in enumerate(d):
            N = []
            for j, jdat in enumerate(idat):
                N.append((((preds_l[i][j] - d[i][j]) / d[i][j]) ** 2) / len(d[i]))
            J.append(np.sqrt(np.sum(N)))
        rRMSE = np.sum(J)

    return rRMSE