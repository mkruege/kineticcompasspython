# SIMULATION OF ITERATIVE APPLICATION OF THE KINETIC COMPASS
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08

import pandas as pd
import numpy as np
import random
import error_th_calibration
import Matlab_Sampling
import ensMeth
from tensorflow import keras
import pickle
import plotting
import seaborn as sns
from matplotlib import pyplot as plt
import time
import os.path
import csv
import copy


def perform_simulation(ens_fits, labeled_ens_fits, iter, model, scaler, n_FE, cmprdata, subcluster_meth=[], subcluster_bin_size=0.5, subcluster_error_bin=0.011, continuous_density=0, continuous_bins=0.05, boundnorm=1, gridsize=[100, 100], gridbounds=[[-6,-2],[10,16]], loggrid=1, x_interp=[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1], set_envs=[1.89E21], envorder=["mutenv0", "mutenv1", "senv0"], remove_ground_truth=0, usecluster=-1, ignore_ES_frame=0, restrict_exp_cond=1, add_uncert=0, env_uncert=[], filter_th=1.3, set_random_experiment=0, useprevious="", exp_distance=0.2, select_gt_by_indx=[], smooth_interp=1, relative_fit_filtering=0, filter_by_exchange=0, filter_by_gt_calibration=0, km_exp=1, error_th_calib=0, exp_iter=0, external_exp=[], makepickle="current.pickle", usepickle=[], exp_pickle=[], specified_filter_pickle=[], lockgrid=[]):
    "Function to simulate application of the Ensemble or Subcluster or Error Calibration Method for multiple iterations with one additional simulated experiment in each round. Can be used with KM3 or a NN surrogate model. Best used with pre-pickled data of outputs for the full grid and individual experiments. For inputs, check specific documentation for simulation inputs."

    ret = 1

    # create a unique name for the directory to save data
    if error_th_calib == 0: #not needed for uncertainty calibration
        timestr = time.strftime("%Y%m%d-%H%M%S_" + str(select_gt_by_indx[0]))
        os.mkdir(timestr)

        # add info about current run to the directory
        info = ["ens_fits", "labeled_ens_fits",  "iter",  "model",  "scaler",  "n_FE", "gridsize",
                "remove_ground_truth",  "usecluster",  "ignore_ES_frame", "add_uncert", "filter_th", "set_random_experiment",
                "useprevious", "exp_distance"]
        vals = [ens_fits, labeled_ens_fits, iter, model, scaler, n_FE, gridsize, remove_ground_truth, usecluster, ignore_ES_frame, add_uncert, filter_th, set_random_experiment, useprevious, exp_distance]
        file_object = open(timestr + '/run_info.txt', 'a')
        for n, row in enumerate(info):
            file_object.write(row + "\n")
            file_object.write(str(vals[n]) + "\n \n")
        file_object.close()

        # create subdirectory and save name in variable
        os.mkdir(timestr + "/iter1")
        savedir = timestr + "/iter1/"


    # load fit ensemble and labels from file
    [fits_params, log_fits_params, fits] = extract_data(ens_fits) #everything is loaded twice, one version for permutations
    [original_fits_params, original_log_fits_params, original_fits] = extract_data(ens_fits) #...and one that remains same
    fit_labels = extract_data(labeled_ens_fits, 1)
    original_fit_labels = extract_data(labeled_ens_fits, 1)

    # get fit ensemble of desired size
    fits_params = fits_params[:n_FE]
    original_fits_params = original_fits_params[:n_FE]
    fits = fits[:n_FE]
    original_fits = original_fits[:n_FE]
    fit_labels = fit_labels[:n_FE]
    original_fit_labels = original_fit_labels[:n_FE]

    # select one fit as "simulated truth"
    [gt_i, gt_params, gt_fit, gt_label] = sample_ground_truth(fits, fits_params, fit_labels, usecluster, select_gt_by_indx)
    print("Selected fit with index " + str(gt_i) + " as simulated truth.")

    # remove simulated truth from data if specified
    if remove_ground_truth == 1:
        fits_params = fits_params.drop(index=gt_i)
        original_fits_params = original_fits_params.drop(index=gt_i)
        fits = fits.drop(index=gt_i)
        original_fits = original_fits.drop(index=gt_i)
        fit_labels = fit_labels.drop(index=gt_i)
        original_fit_labels = original_fit_labels.drop(index=gt_i)
        fits_params = fits_params.reset_index(drop=True)
        original_fits_params = original_fits_params.reset_index(drop=True)
        fits = fits.reset_index(drop=True)
        original_fits = original_fits.reset_index(drop=True)
        fit_labels = fit_labels.reset_index(drop=True)
        original_fit_labels = original_fit_labels.reset_index(drop=True)

    # get the number of current keeps (initial)
    if remove_ground_truth == 0:
        n_keeps = [n_FE]
    else:
        n_keeps = [n_FE-1]

    cluster1 = [len(fit_labels[fit_labels == 1])]
    cluster0 = [len(fit_labels[fit_labels == 0])]

    error_collect = [] #for error calibration
    rRMSEs = [] #list for errors
    rad_X = [] #list for env. parameters of simulated experiments

    # loop over simulation iterations
    for i in range(iter):
        print("Iteration " + str(i+1) + " of " + str(iter) + ".") #display progress
        if i > 0 and error_th_calib == 0: #change directory name for iteration
            os.mkdir(timestr + "/iter" + str(i+1))
            savedir = savedir[:-6] + "iter" + str(i+1) + "/"

        if len(external_exp) == 0:
            if set_random_experiment == 0:
                #perform ensemble method to determine Ensemble Spread
                if i == 0:
                    if len(useprevious) == 0: # full ensemble method is performed in first run if no provided file
                        if remove_ground_truth == 1:
                            ret = ensMeth.perform_ensemble_meth("not_relevant", n_FE-1, gridsize, gridbounds, model, scaler, loggrid=loggrid, subcluster_meth=subcluster_meth, subcluster_bin_size=subcluster_bin_size, subcluster_error_bin=subcluster_error_bin, continuous_density=continuous_density, continuous_bins=continuous_bins, both=1, fulldata=[fits_params, fit_labels], savedir=savedir, smooth_interp=smooth_interp, use_pickle=usepickle, make_pickle=makepickle, set_envs=set_envs, envorder=envorder, x_interp=x_interp, boundnorm=boundnorm)
                        else:
                            ret = ensMeth.perform_ensemble_meth("not_relevant", n_FE, gridsize, gridbounds, model, scaler, loggrid=loggrid, subcluster_meth=subcluster_meth, subcluster_bin_size=subcluster_bin_size, subcluster_error_bin=subcluster_error_bin, continuous_density=continuous_density, continuous_bins=continuous_bins, both=1, fulldata=[fits_params, fit_labels], savedir=savedir, smooth_interp=smooth_interp, use_pickle=usepickle, make_pickle=makepickle, set_envs=set_envs, envorder=envorder, x_interp=x_interp, boundnorm=boundnorm)
                else: # full ensemble method is performed in following runs anyway
                    try:
                        ret = ensMeth.perform_ensemble_meth("not_relevant", fits_params.shape[0], gridsize, gridbounds, model, scaler, loggrid=loggrid, subcluster_meth=subcluster_meth, subcluster_bin_size=subcluster_bin_size, subcluster_error_bin=subcluster_error_bin, continuous_density=continuous_density, continuous_bins=continuous_bins, both=1, fulldata=[fits_params, fit_labels], savedir=savedir, smooth_interp=smooth_interp, use_pickle=usepickle, make_pickle=makepickle, set_envs=set_envs, envorder=envorder, x_interp=x_interp, boundnorm=boundnorm)
                    except:
                        ret = ensMeth.perform_ensemble_meth("not_relevant", fits_params.shape[0], gridsize, gridbounds, model, scaler, loggrid=loggrid, subcluster_meth=subcluster_meth, subcluster_bin_size=subcluster_bin_size, subcluster_error_bin=subcluster_error_bin, continuous_density=continuous_density, continuous_bins=continuous_bins, both=1, fulldata=[fits_params, fit_labels], savedir=savedir, smooth_interp=smooth_interp, use_pickle=usepickle, make_pickle=makepickle, set_envs=set_envs, envorder=envorder, x_interp=x_interp, boundnorm=boundnorm)

            #terminate simulation if only one fit is left
            if fits_params.shape[0] < 2 or ret == 0:
                print("Only one or no fits left in ensemble - terminating simulation.")
                break

            # select env. parameters for simulated experiment
            if i == 0 and len(useprevious) != 0:
                rad_X.append(select_experiment(ignore_ES_frame, set_random_experiment, exp_distance, rad_X, savedir, restrict_exp_cond, gridbounds, useprev=useprevious, lockgrid=lockgrid))
            else:
                rad_X.append(select_experiment(ignore_ES_frame, set_random_experiment, exp_distance, rad_X, savedir, restrict_exp_cond, gridbounds, lockgrid=lockgrid))

        else: # or get env. parameters provided
            rad_X = external_exp[:i+1]

        if error_th_calib == 0:
            # simulate experiment with given model
            curr_out = simulate_experiment(gt_i, gt_params, rad_X[-1][0], rad_X[-1][1], model, scaler, add_uncert, restrict_exp_cond, ignore_ES_frame, set_random_experiment, exp_distance, rad_X, savedir, useprevious, km_exp, env_uncert, exp_pickle, gridsize, gridbounds, loggrid, lockgrid)
            if i == 0: # collect experiments for each simulation round
                sim_exp = curr_out[0]
            else:
                sim_exp = np.append(sim_exp, curr_out[0], axis=0)
            if len(curr_out[1]) != 0: # replace last set of env. params if previously chosen experiment is not accepted due to time limit
                rad_X = rad_X[:i]
                rad_X.append(curr_out[1])

            # filter ensemble fits for new rRMSE

            #old way:
            #[fits_params, fits, fit_labels, fits_params_d, fits_d, fit_labels_d, keeps, drops, curr_rRMSEs] = filter_ensemble_fits(fits_params, fits, fit_labels, sim_exp, model, scaler, rad_X, filter_th, i, savedir, relative_fit_filtering, filter_by_exchange, filter_by_gt_calibration, gt_i, usepickle, makepickle, gridsize)
            #now throws Key Error (fit selection) - not needed anyway

            #better to revive fits, gives same information after all, and more
            if len(specified_filter_pickle) == 0:
                [fits_params, fits, fit_labels, fits_params_d, fits_d, fit_labels_d, keeps, drops, curr_rRMSEs] = filter_ensemble_fits(original_fits_params, original_fits_params, original_fit_labels, sim_exp, model, scaler, rad_X, filter_th, i, savedir, relative_fit_filtering, filter_by_exchange, filter_by_gt_calibration, gt_i, usepickle, makepickle, gridsize, gridbounds, loggrid, cmprdata, x_interp)

            else: # for second hybrid-approach: use NN for ensemble method but KM-SUB for fit filtering
                [fits_params, fits, fit_labels, fits_params_d, fits_d, fit_labels_d, keeps, drops,
                 curr_rRMSEs] = filter_ensemble_fits(original_fits_params, original_fits_params, original_fit_labels,
                                                     sim_exp, model, scaler, rad_X, filter_th, i, savedir,
                                                     relative_fit_filtering, filter_by_exchange,
                                                     filter_by_gt_calibration, gt_i, specified_filter_pickle, makepickle, gridsize,
                                                     gridbounds, loggrid, cmprdata, x_interp)

            rRMSEs.append(curr_rRMSEs)

            # save keeps to file
            keeplist = []
            for ij in range(n_FE):
                if ij in keeps:
                    keeplist.append(1)
                else:
                    keeplist.append(0)

            if i == 0: #initialize keep file to track accepted fits in each simulation round
                with open(timestr + '/keeps.csv', 'w') as f:
                    # create the csv writer
                    writer = csv.writer(f)
                    writer.writerow([str(x) for x in range(n_FE)])
                    writer.writerow(keeplist)
            else: #append to keep file
                with open(timestr + '/keeps.csv', 'a') as f:
                    # create the csv writer
                    writer = csv.writer(f)
                    writer.writerow(keeplist)

            # analyse results of current iteration
            n_keeps.append(fits_params.shape[0])
            cluster0.append(len(fit_labels[fit_labels == 0]))
            cluster1.append(len(fit_labels[fit_labels == 1]))
            if set_random_experiment == 0:
                analyse_iteration(fits_params, fits, fit_labels, fits_params_d, fits_d, fit_labels_d, rad_X, i, savedir, useprevious, gridsize, gridbounds, rRMSEs, cmprdata, lockgrid)

        else: #error calibration to calibrate uncertainties for env. parameters and measurements as well as filter function
            if i == 0: # get errors for original experiments
                errors = [error_th_calibration.perform_error_calibration(exp_iter, gt_i, gt_params,
                                                           model, scaler, add_uncert, restrict_exp_cond,
                                                           ignore_ES_frame, exp_distance, rad_X, km_exp, env_uncert, i, cmprdata, gridsize, gridbounds, loggrid, lockgrid)]
                error_std = [0]

            #get partial error for each simulated experiment
            partial = error_th_calibration.perform_error_calibration(exp_iter, gt_i, gt_params,
                                                           model, scaler, add_uncert, restrict_exp_cond,
                                                           ignore_ES_frame, exp_distance, rad_X, km_exp, env_uncert, i+1, cmprdata, gridsize, gridbounds, loggrid, lockgrid)

            # collect return values
            error_collect.append(partial)
            errors.append(np.average(partial))
            error_std.append(np.std(partial))

    if error_th_calib == 0:
        #return results of full run
        print("Simulated Truth:")
        print("Params: " + str(gt_fit))
        print("Label " + str(gt_label))

        print("Iteration progresses:")
        print("Ensemble Fit Size: " + str(n_keeps))
        print("Cluster 0: " + str(cluster0))
        print("Cluster 1: " + str(cluster1))

        print("Simulated expriments:")
        print(rad_X)
        print(sim_exp)
        endinfo = ["Simulated Truth Params", "Simulated Truth Label", "Ensemble Fit Sizes", "Cluster 0", "Cluster 1", "Simulated experiment input", "Simulated experiment output"]
        endvals = [gt_fit, gt_label, n_keeps, cluster0, cluster1, rad_X, sim_exp]
        file_object = open(timestr + '/run_info.txt', 'a')
        for n, row in enumerate(endinfo):
            file_object.write(row + "\n")
            file_object.write(str(endvals[n]) + "\n \n")
        file_object.close()

        return timestr, endinfo, endvals

    else: #error calibration
        return errors, error_std, error_collect

def analyse_iteration(fits_params, fits, fit_labels, fits_params_d, fits_d, fit_labels_d, radX, run, savedir, useprevious, gridsize, gridbounds, rRMSEs, cmprdata, lockgrid):
    "Function to analyse and plot simulation results of one iteration: contour plots, plot matrices."

    print("Analysing and plotting simulation results for current iteration.")
    if len(useprevious) == 0 or run != 0:
        #contour plots for current ES
        plotting.make_contour_plot(savedir + "ensembe_spreads_matrix.csv", cmprdata, xt=[-6, -5.5, -5, -4.5, -4, -3.5, -3, -2.5, -2], yt=[10, 11, 12, 13, 14, 15, 16], add_exp=radX, savedir=savedir, run=run, lockgrid=lockgrid, vmax=2.5)
        #plotting.make_contour_plot(savedir + "ensembe_spreads_cluster_mean_diff_matrix.csv", cmprdata, xt=[-6, -5.5, -5, -4.5, -4, -3.5, -3, -2.5, -2], yt=[10, 11, 12, 13, 14, 15, 16], add_exp=radX, savedir=savedir, run=run, gridsize=gridsize)
        #plotting.make_contour_plot(savedir + "ensembe_spreads_cluster_mean_diff_matrix.csv", cmprdata, log=1, xt=[-6, -5.5, -5, -4.5, -4, -3.5, -3, -2.5, -2], yt=[10, 11, 12, 13, 14, 15, 16], add_exp=radX, savedir=savedir, run=run, gridsize=gridsize)
    else:
        plotting.make_contour_plot(useprevious, cmprdata,
                                   xt=[-6, -5.5, -5, -4.5, -4, -3.5, -3, -2.5, -2], yt=[10, 11, 12, 13, 14, 15, 16],
                                   add_exp=radX, savedir=savedir, run=run, lockgrid=lockgrid, vmax=2.5)
        #plotting.make_contour_plot(useprevious[:-10] + "cluster_mean_diff_matrix.csv", cmprdata,
        #                           xt=[-6, -5.5, -5, -4.5, -4, -3.5, -3, -2.5, -2], yt=[10, 11, 12, 13, 14, 15, 16],
        #                           add_exp=radX, savedir=savedir, run=run, gridsize=gridsize)
        #plotting.make_contour_plot(useprevious[:-10] + "cluster_mean_diff_matrix.csv", cmprdata, log=1,
        #                           xt=[-6, -5.5, -5, -4.5, -4, -3.5, -3, -2.5, -2], yt=[10, 11, 12, 13, 14, 15, 16],
        #                           add_exp=radX, savedir=savedir, run=run, gridsize=gridsize)


    # scattermatrix for keeps
    #log_fits_params = fits_params.apply(np.log10)
    #log_fits_params["label"] = fit_labels
    #sns.pairplot(log_fits_params, hue="label")
    #plt.savefig(savedir + "PlotMatrix_" + str(run) + ".pdf")
    #plt.show()
    #plt.close()

    # scattermatrix blanc
    #log_fits_params = fits_params.apply(np.log10)
    #log_fits_params_d = fits_params_d.apply(np.log10)
    #log_fits_params_all = pd.concat([log_fits_params, log_fits_params_d], axis=0, ignore_index=True)
    #sns.set(font_scale=2)
    #sns.pairplot(log_fits_params_all)
    #plt.savefig(savedir + "BlancPlotMatrix_" + str(run) + ".pdf")
    #plt.show()
    #plt.close()
    #sns.reset_defaults()

    #scattermatrix with density
    #log_fits_params = fits_params.apply(np.log10)
    #log_fits_params_d = fits_params_d.apply(np.log10)
    #log_fits_params_all = pd.concat([log_fits_params, log_fits_params_d], axis=0, ignore_index=True)
    #random_indices = random.sample(range(len(log_fits_params_all)), 5)
    #df1_sample = log_fits_params_all.iloc[random_indices]
    #g3 = sns.PairGrid(df1_sample)
    #colors = sns.color_palette('tab10')[:5]
    #g3.map_upper(plt.scatter, s=100, color=colors)
    ##g3.map_lower(sns.kdeplot, cmap="coolwarm", shade=True)
    #g3.map_lower(plt.scatter, s=100, color=colors)
    #g3.map_diag(plt.hist, color="blue", edgecolor="black")
    #plt.rcParams.update({'font.size': 22})
    #plt.savefig("pairplot_sample.pdf")
    #plt.show()

    # scattermatrix for drops
    #log_fits_params_d = fits_params_d.apply(np.log10)
    #log_fits_params_d["label"] = fit_labels_d
    #sns.pairplot(log_fits_params_d, hue="label")
    #plt.savefig(savedir + "Dropped_PlotMatrix_" + str(run) + ".png")
    #plt.show()
    #plt.close()


    # scattermatrix for drops
    log_fits_params_k = fits_params.apply(np.log10)
    log_fits_params_d = fits_params_d.apply(np.log10)
    log_fits_params_k["keeps"] = [1 for x in range(log_fits_params_k.shape[0])]
    log_fits_params_d["keeps"] = [0 for x in range(log_fits_params_d.shape[0])]
    log_fits_params_kd = log_fits_params_k.append(log_fits_params_d)
    log_fits_params_kd = log_fits_params_kd.reset_index(drop=True)
    sns.pairplot(log_fits_params_kd, hue="keeps")
    plt.savefig(savedir + "KeepDrop_PlotMatrix_" + str(run) + ".pdf")
    plt.show()
    plt.close()
    sns.reset_defaults()

    #only KDE plots
    #log_fits_params_kd_subset = log_fits_params_kd.iloc[:, :5]
    #fig, axes = plt.subplots(nrows=1, ncols=5, figsize=(15, 3), sharey=True)
    #for i, ax in enumerate(axes.flatten()):
    #    if i < 5:
    #        sns.kdeplot(log_fits_params_kd_subset.iloc[:, i], ax=ax, shade=True, color="blue")
    #        sns.kdeplot(log_fits_params_kd_subset[log_fits_params_kd["keeps"] == 1].iloc[:, i], ax=ax, shade=True,
    #                    color="red")
    #        ax.set_xlabel(log_fits_params_kd_subset.columns[i])
    #fig.legend(labels=['All data', 'keeps=1'], loc='lower center', bbox_to_anchor=(0.5, -0.15), ncol=2)
    #plt.tight_layout()
    #plt.savefig(savedir + "KDE_Plots_" + str(run) + ".pdf")
    #plt.show()
    #plt.close()

    #only histos
    #log_fits_params_kd_subset = log_fits_params_kd.iloc[:, :5]
    #fig, axes = plt.subplots(nrows=1, ncols=5, figsize=(15, 3), sharey=True)
    #for i, ax in enumerate(axes.flatten()):
    #    if i < 5:
    #        sns.histplot(log_fits_params_kd_subset.iloc[:, i], ax=ax, kde=False, color="blue", alpha=0.5)
    #        sns.histplot(log_fits_params_kd_subset[log_fits_params_kd["keeps"] == 1].iloc[:, i], ax=ax, kde=False,
    #                     color="red", alpha=0.5)
    #        ax.set_xlabel(log_fits_params_kd_subset.columns[i])
    #        ax.set_ylabel("Count")
    #fig.legend(labels=['All data', 'keeps=1'], loc='lower center', bbox_to_anchor=(0.5, -0.15), ncol=2)
    #plt.tight_layout()
    #plt.savefig(savedir + "Hist_Plots_" + str(run) + ".pdf")
    #plt.show()
    #plt.close()

    #KDE+hist plot
    #fig, axes = plt.subplots(nrows=1, ncols=5, figsize=(15, 3), sharey=True)
    #for i, ax in enumerate(axes.flatten()):
    #    sns.histplot(log_fits_params_kd.iloc[:, i], ax=ax, kde=True, color="blue", alpha=0.5)
    #    sns.histplot(log_fits_params_kd[log_fits_params_kd["keeps"] == 1].iloc[:, i], ax=ax, kde=True, color="red",
    #                 alpha=0.5)
    #    ax.set_xlabel(log_fits_params_kd.columns[i])
    #    ax.set_ylabel("Counts")
    #    #ax.legend(labels=['All data', 'keeps=1'], loc='upper right')
    #plt.tight_layout()
    #plt.savefig(savedir + "KDE_Plots_" + str(run) + ".pdf")
    #plt.show()
    #plt.close()


    fig, axes = plt.subplots(nrows=1, ncols=5, figsize=(15, 3), sharey=True)
    for i, ax in enumerate(axes.flatten()):
        data_min = np.min(log_fits_params_kd.iloc[:, i])
        data_max = np.max(log_fits_params_kd.iloc[:, i])
        bin_width = (data_max - data_min) / 20  # 20 bins
        bins = np.arange(data_min, data_max + bin_width, bin_width)
        sns.histplot(log_fits_params_kd.iloc[:, i], ax=ax, bins=bins, kde=True, color="blue", alpha=0.5)
        sns.histplot(log_fits_params_kd[log_fits_params_kd["keeps"] == 1].iloc[:, i], ax=ax, bins=bins, kde=True,
                     color="red", alpha=0.5)
        ax.set_xlabel(log_fits_params_kd.columns[i])
        ax.set_ylabel("Counts")

    plt.tight_layout()
    plt.savefig(savedir + "KDE_Plots_" + str(run) + ".pdf")
    plt.show()
    plt.close()
    sns.reset_defaults()

    #distribution but normalized
    #log_fits_params_kd_subset = log_fits_params_kd.iloc[:, :5]
    #fig, axes = plt.subplots(nrows=1, ncols=5, figsize=(15, 3), sharey=True)
    #for i, ax in enumerate(axes.flatten()):
    #    if i < 5:
    #        sns.kdeplot(log_fits_params_kd_subset.iloc[:, i], ax=ax, shade=True, color="blue")
    #        sns.kdeplot(log_fits_params_kd_subset[log_fits_params_kd["keeps"] == 1].iloc[:, i], ax=ax, shade=True,
    #                    color="red", alpha=0.5, bw_adjust=0.5)
    #        #sns.kdeplot(log_fits_params_kd_subset[log_fits_params_kd["keeps"] == 0].iloc[:, i], ax=ax, shade=True,
    #        #            color="green", alpha=0.5, bw_adjust=0.5)
    #        ax.set_xlabel(log_fits_params_kd_subset.columns[i])
    #fig.legend(labels=['All data', 'keeps=1', 'keeps=0'], loc='lower center', bbox_to_anchor=(0.5, -0.15), ncol=3)
    #plt.tight_layout()
    #plt.savefig(savedir + "KDE_Plots_" + str(run) + ".pdf")
    #plt.show()
    #plt.close()

    #log_fits_params_kd_subset = log_fits_params_kd.iloc[:, :5]
    #fig, axes = plt.subplots(nrows=1, ncols=5, figsize=(15, 3), sharey=True)
    #for i, ax in enumerate(axes.flatten()):
    #    if i < 5:
    #        all_data = log_fits_params_kd_subset.iloc[:, i].values
    #        keeps_data = log_fits_params_kd_subset[log_fits_params_kd["keeps"] == 1].iloc[:, i].values
    #        all_data_counts, all_data_bins = np.histogram(all_data, bins=50)
    #        keeps_data_counts, keeps_data_bins = np.histogram(keeps_data, bins=all_data_bins)
    #        sns.lineplot(x=(all_data_bins[:-1] + all_data_bins[1:]) / 2, y=all_data_counts, ax=ax, color="blue")
    #        sns.lineplot(x=(keeps_data_bins[:-1] + keeps_data_bins[1:]) / 2, y=keeps_data_counts, ax=ax, color="red")
    #        ax.set_xlabel(log_fits_params_kd_subset.columns[i])
    #    else:
    #        fig.delaxes(ax)
    #fig.legend(labels=['All data', 'keeps=1'], loc='lower center', bbox_to_anchor=(0.5, -0.15), ncol=2)
    #plt.tight_layout()
    #plt.savefig(savedir + "KDE_Plots_" + str(run) + ".pdf")
    #plt.show()
    #plt.close()

    plt.boxplot(rRMSEs)
    plt.savefig(savedir + "rRMSEs_boxplot_" + str(run) + ".png")
    #plt.show()
    plt.close()

    return

def filter_ensemble_fits(fits_params, fits, fit_labels, sim_exp, model, scaler, rad_X, filter_th, run, savedir, relative, byexchange, gt_calibration, gt_i, usepickle, makepickle, gridsize, gridbounds, loggrid, cmprdata, x_interp):
    "Function to filter all fits in fit ensemble based on a new simulated experiment (and previous ones). Has multiple modes, check documentation for simulation inputs. gt_calibration >(overrules) byexchange > relative > filter_th."

    print("Filtering fit ensemble based on simulated experiment.")

    if len(usepickle) != 0:
        try:
            pickle.load(open("exp_preds_" + usepickle[0], "rb"))
        except:
            makepickle = usepickle[0]
            usepickle = []

    # collect comparedata (in and out)
    cmprdata_in = copy.deepcopy(cmprdata[0])
    cmprdata_out_d = copy.deepcopy(cmprdata[1][0])
    cmprdata_out_t = copy.deepcopy(cmprdata[1][1])

    # append simulated experiment outputs
    for sexp in sim_exp:
        cmprdata_out_t.append(sexp.tolist())
        cmprdata_out_d.append([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9])

    # append remaining environmental parameter for model prediction
    for pair in rad_X:
        cmprdata_in.append(pair + [1.89E+21])

    # loop over all fit parameters, append data for different experiments (simulated and real), calculate new rRMSE
    rRMSEs = []
    if len(usepickle) == 0: #no pickled data provided, sample with currently used model
        print("No experimental model prediction pickle provided. Computing new one in 500*7 steps...")
        fits_params = fits_params.to_numpy()
        curr_preds = []
        for fit_param in fits_params:
            exp_params = []
            for trip in cmprdata_in:
                exp_params.append(np.append(fit_param, trip))

            if "km3" not in model: #use nn model for prediction
                # LOAD NN FORWARD MODEL
                NNmodel = keras.models.load_model(model)

                # LOAD x- and y-scaler associated to this model
                [scaler_x, scaler_y] = pickle.load(open(scaler, "rb"))

                # transform input parameters and run NN model for prediction
                inpts_log = np.log10(exp_params)
                inpts_t = scaler_x.transform(inpts_log)
                pred = NNmodel.predict(inpts_t)
                pred_t = scaler_y.inverse_transform(pred)
                pred_f = 10 ** pred_t
                curr_preds.append(pred_f)
            else:
                #sample with matlab engine and KM3 (SLOW!)
                curr_preds.append(Matlab_Sampling.get_KM3_multisample(exp_params, len(exp_params), [], 5, returnouts=1))

        # when no pickle was present, make one now in first round for future iterations and runs (doesn't change for same model and initial set of experiments)
        if len(sim_exp) == 1:
            pickle.dump([x[:-1] for x in curr_preds], open("exp_preds_" + makepickle, "wb"))


    else: #load the provided pickle for experiment predictions
        curr_preds_params = []
        curr_preds = pickle.load(open("exp_preds_" + usepickle[0], "rb"))
        [all_preds_params, all_preds] = pickle.load(open(usepickle[0], "rb"))  # load specified pickled grid



        [radgrid, Xgrid] = ensMeth.get_grid(gridsize, gridbounds, loggrid=loggrid)  # load current grid
        # compare grid values with pickled params to find correct index of pred
        for [rad, X, Y] in cmprdata_in:
            step = -1
            ok = 0
            for nr, r in enumerate(radgrid):
                for ni, x in enumerate(Xgrid):
                    step = step + 1
                    if np.isclose(rad, r, rtol=1E-3) and np.isclose(X, x, rtol=1E-3): #use isclose to avoid errors due to rounding
                        curr_preds_params.append(all_preds_params[step])
                        for i in range(len(curr_preds)):
                            curr_preds[i] = np.append(curr_preds[i], all_preds[step][i].reshape((1,9)), axis=0)
                        ok = 1
                        break # if correct position is found, break both loops
                if ok == 1:
                    break

    drops = [] #obtain drops from one of the filtering methods

    if gt_calibration != 0: #calibration by ground truth using leave-one-out principle
        gt_pred=curr_preds[gt_i]
        try:
            gt_calibr_error = calc_error_Xsets(sim_exp, gt_pred.tolist(), x_interp, cmprdata=cmprdata) #the error associated with the experimental uncertainty (comparison with vanilla pred of ground truth
        except:
            gt_calibr_error = calc_error_Xsets(sim_exp, gt_pred, x_interp, cmprdata=cmprdata)

        for i, preds in enumerate(curr_preds):
            try:
                fit_error = calc_error_Xsets(sim_exp, preds, x_interp, cmprdata=cmprdata)  # the error associated to the fit in question
            except:
                fit_error = calc_error_Xsets(sim_exp, preds.tolist(), x_interp, cmprdata=cmprdata)

            rel_filter_th = gt_calibr_error + gt_calibr_error * gt_calibration
            if fit_error > rel_filter_th:
                drops.append(i)

    elif byexchange != 0: #filter fits by error echange method
        for i, preds in enumerate(curr_preds):
            due = np.round((len(preds)-1)*byexchange) # due is the number of iterations where leave-out-one-error should be smaller than full error
            try:
                original_error = calc_error_Xsets(sim_exp, preds, x_interp, cmprdata=cmprdata) # the error associated to all experiments, previous and current
            except:
                original_error = calc_error_Xsets(sim_exp, preds.tolist(), x_interp, cmprdata=cmprdata)
            for j in range(len(preds)-1):
                curr_error = calc_error_Xsets(sim_exp, preds, x_interp, leaveout=[j], cmprdata=cmprdata) # leave-out-one error (only removing previous experiments)
                if original_error > curr_error:
                    due = due - 1
            if due > 0: # if due is not satisfied at the end, this fit is dropped
                drops.append(i)


    else: #filter fits by calculating new error and filtering fits based on best fit (relative) or with absolute threshold
        for pred_f in curr_preds:
            rRMSEs.append(calc_error_Xsets(sim_exp, pred_f, x_interp, cmprdata=cmprdata))


        # with given rRMSEs, filter all parameter sets that exceed specified error threshold
        if relative == 0: # strict error threshold
            for i, rRMSE in enumerate(rRMSEs):
                if rRMSE > filter_th:
                    drops.append(i)

        else:# relative to best fit (often simulated truth)
            best_rRMSE = min(rRMSEs)
            rel_filter_th = best_rRMSE + best_rRMSE*relative
            for i, rRMSE in enumerate(rRMSEs):
                if rRMSE > rel_filter_th:
                    drops.append(i)

    keeps = [] # save the keeps
    for i in range(fits_params.shape[0]):
        if i not in drops:
            keeps.append(i)


    # plot extrapolated drops and keeps
    plot_exp_preds(curr_preds, cmprdata_out_d, cmprdata_out_t, keeps, drops, fit_labels, run, savedir, x_interp)
    #plot_exp_preds(curr_preds, cmprdata_out_d, cmprdata_out_t, keeps, drops, fit_labels, run, savedir, restrict=200)


    # drop parameter sets
    fits_params = pd.DataFrame(fits_params)
    fits_params_k = fits_params.drop(index=drops)
    fits_k = fits.drop(index=drops)
    fit_labels_k = fit_labels.drop(index=drops)

    # and reset indices
    fits_params_k = fits_params_k.reset_index(drop=True)
    fits_k = fits_k.reset_index(drop=True)
    fit_labels_k = fit_labels_k.reset_index(drop=True)

    # repeat with keeps to investigate drops
    # drop parameter sets
    fits_params_d = fits_params.drop(index=keeps)
    fits_d = fits.drop(index=keeps)
    fit_labels_d = fit_labels.drop(index=keeps)

    # and reset indices
    fits_params_d = fits_params_d.reset_index(drop=True)
    fits_d = fits_d.reset_index(drop=True)
    fit_labels_d = fit_labels_d.reset_index(drop=True)

    print("Accepted fits: " + str(len(keeps)))
    print("Rejected fits: " + str(len(drops)))

    return [fits_params_k, fits_k, fit_labels_k, fits_params_d, fits_d, fit_labels_d, keeps, drops, rRMSEs]

def plot_exp_preds(preds, exp_d, exp_t, keeps, drops, labels, run, savedir, x_interp, restrict=0, showclusters=0):
    "Function to plot model prediction and laboratory data from real or simulated experiments."

    # format...
    preds_t = []
    for i in range(len(preds[0])):
        curr = []
        for pred in preds:
            curr.append(pred[i])
        preds_t.append(curr)

    # interpolate fits by their 9 values
    interpols = []
    for pred in preds_t:
        int = []
        for p in pred:
            int.append(ensMeth.interpolate(p, 0.01, x_interp))
        interpols.append(int)

    if showclusters != 0:
        # iterate over interpolations and plot
        for num, preds in enumerate(interpols):
            for i, pred in enumerate(preds):
                if restrict != 0: # if too many fits in plot, restrict their number
                    if i == restrict:
                        break
                if i not in keeps:
                    if labels[i] == 1:
                        plt.plot(pred, np.arange(0, 1, 0.01), c="grey", linewidth=0.1, zorder=0)
                    else:
                        plt.plot(pred, np.arange(0, 1, 0.01), c="pink", linewidth=0.1, zorder=0)
                else:
                    if labels[i] == 1:
                        plt.plot(pred, np.arange(0, 1, 0.01), c="blue", linewidth=0.1, zorder=0)
                    else:
                        plt.plot(pred, np.arange(0, 1, 0.01), c="green", linewidth=0.1, zorder=0)
            try:
                try:
                    plt.scatter(exp_t[num], exp_d[num], marker="o", color="black", zorder=1)
                except:
                    plt.scatter(exp_t[num], exp_d[num], marker="o", color="black", zorder=1)
            except:
                try:
                    plt.scatter(exp_t[num], exp_d[num], marker="o", color="black", zorder=1)
                except:
                    plt.scatter(exp_t[num][5:], exp_d[num], marker="o", color="black", zorder=1)
            plt.ylabel("Normalized concentration", fontsize=14)
            plt.xlabel("Time (s)", fontsize=14)
            plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
            plt.tick_params(axis='both', which='major', labelsize=14)
            plt.savefig(savedir + "run_" + str(run) + "_exp_" + str(num) + "_res_" + str(restrict) + ".pdf")
            #plt.show()
            plt.close()

    else:
        # iterate over interpolations and plot
        for num, preds in enumerate(interpols):
            for i, pred in enumerate(preds):
                if restrict != 0:  # if too many fits in plot, restrict their number
                    if i == restrict:
                        break
                if i not in keeps:
                    if labels[i] == 1:
                        plt.plot(pred, np.arange(0, 1, 0.01), c="grey", linewidth=0.1, zorder=0)
                    else:
                        plt.plot(pred, np.arange(0, 1, 0.01), c="grey", linewidth=0.1, zorder=0)
                else:
                    if labels[i] == 1:
                        plt.plot(pred, np.arange(0, 1, 0.01), c="green", linewidth=0.2, zorder=1)
                    else:
                        plt.plot(pred, np.arange(0, 1, 0.01), c="green", linewidth=0.2, zorder=1)
            try:
                try:
                    plt.scatter(exp_t[num], exp_d[num], marker="o", color="black", zorder=2)
                except:
                    plt.scatter(exp_t[num], exp_d[num], marker="o", color="black", zorder=2)
            except:
                try:
                    plt.scatter(exp_t[num], exp_d[num], marker="o", color="black", zorder=2)
                except:
                    plt.scatter(exp_t[num][5:], exp_d[num], marker="o", color="black", zorder=2)

            plt.ylabel("Normalized concentration", fontsize=14)
            plt.xlabel("Time (s)", fontsize=14)
            plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
            plt.tick_params(axis='both', which='major', labelsize=14)
            plt.savefig(savedir + "run_" + str(run) + "_exp_" + str(num) + "_res_" + str(restrict) + ".pdf")
            # plt.show()
            plt.close()

    return

def calc_error_Xsets(sim_exp, preds, x_interp=[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1], logabs=1, leaveout=[], cmprdata = []):
    "Calculates relative root mean square error in comparison with X data sets (the ones provided in cmprdata and sim_exp, compared with preds). Leaveout can remove one specified experiment and logabs=0 sets rRMSE error calculation."

    if len(cmprdata) == 0: #no comparison data provided (probably non-updated code)
        print("Warning! No experimental data for comparison provided. Hard-coded data used. Please make sure that this is desired or use partial error method in error_th_calibration.")
        cmprdata_out_d = [list(reversed([0.604, 0.721, 0.427, 0.558, 0.11, 0.144])),
                          list(reversed([0.676, 0.822, 0.732, 0.676, 0.188, 0.204, 0.0962, 0.106])),
                          list(reversed([0.975156, 0.938477, 0.8836, 0.855625, 0.81, 0.765625, 0.7921, 0.680625, 0.650039, 0.600625,
                           0.543906, 0.525625])),
                          list(reversed([0.91, 0.792, 0.724, 0.595, 0.522, 0.384, 0.291, 0.227])), list(reversed([0.8673, 0.6122, 0.4468, 0.1785])),
                          list(reversed([0.7858, 0.4942, 0.2664])), list(reversed([0.5091, 0.2745, 0.1077]))]

        cmprdata_out_t = [list(reversed([240.108, 240.108, 398.374, 398.374, 899.729, 899.729])),
                          list(reversed([9000, 9000, 18000, 18000, 67500, 67500, 80500, 80500])),
                          list(reversed([1.3, 2.5, 4.1, 5.2, 6.38, 7.5, 9, 10.3, 11.5, 13.1, 14.3, 15.4])),
                          list(reversed([0.2313, 0.4579, 0.7202, 0.9292, 1.2205, 1.6982, 2.1938, 2.6898])),
                          list(reversed([3.3, 7.2, 14.7, 35.24])), list(reversed([3.06, 6.65, 14.57])), list(reversed([2.8, 6.75, 14.55]))]

    else: # collect relevant data
        cmprdata_out_d = copy.deepcopy(cmprdata[1][0])
        cmprdata_out_t = copy.deepcopy(cmprdata[1][1])

    # obtain interpolations for predictions
    interpol_preds = []
    for pred in preds:
        interpol_preds.append(ensMeth.interpolate(pred, 0.001, x_interp))

    # append new experiment to experimental data including decomposition steps
    data = cmprdata_out_t
    for exp in sim_exp:
        data.append(exp.tolist())
        cmprdata_out_d.append([0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9])

    # save experimental data in lists
    d = []
    for row in data:
        dr = list(row)
        d.append(dr)

    # get model values as lists from interpolations
    preds_l = []
    for rnum, row in enumerate(interpol_preds[:7]):
        curr = []
        for cd in cmprdata_out_d[rnum]:
            curr.append(round(cd*1000))
        preds_l.append([row[x] for x in curr])

    # transform to lists if arrays still present
    try:
        for row in preds[7:]:
            preds_l.append(row.tolist())
    except:
        pass

    # remove specified experiment from predictions and experimental data
    if len(leaveout) != 0:
        d = [x for n, x in enumerate(d) if n not in leaveout]
        preds_l = [x for n, x in enumerate(preds_l) if n not in leaveout]

    # not necessary with new data type and error calc
    #leaveout = []
    #for n, dat in enumerate(d):
    #    while "no_data" in dat:
    #        leaveout.append([n, dat.index("no_data")])
    #        del preds_l[n][dat.index("no_data")]
    #        del d[n][dat.index("no_data")]

    if logabs == 1: #mean square logarithmic error
        J = []
        for i, idat in enumerate(d):
            N = []
            for j, jdat in enumerate(idat):
                N.append((np.log10(preds_l[i][j]) - np.log10(d[i][j])) ** 2)
            J.append(np.average(N))
        rRMSE = np.average(J)

    else: #relative root mean square error
        J = []
        for i, idat in enumerate(d):
            N = []
            for j, jdat in enumerate(idat):
                N.append((((preds_l[i][j] - d[i][j]) / d[i][j]) ** 2) / len(d[i]))
            J.append(np.sqrt(np.sum(N)))
        rRMSE = np.sum(J)


    return rRMSE

def simulate_experiment(gt_i, gt_params, rad, X, model, scaler, add_uncert, restrict_exp_cond, ignore_ES_frame, random_exp, exp_distance, rad_X, savedir, useprev, km_exp, env_uncert, exp_pickle, gridsize, gridbounds, loggrid, lockgrid, lockbest=0):
    "Function to simulate a previously selected experiment under consideration of uncertainty. If conditions don't allow the current experiment, a new experiment is recursively selected until the selection succeeded."

    #print("Simulating experiment for rad: " + str(rad) + " and X0: " + str(X))

    if len(exp_pickle) == 0: #if experiment is simulated with predictive model
        if km_exp == 0: #if experiment is simulated with KM3
            # LOAD NN FORWARD MODEL
            NNmodel = keras.models.load_model(model)

            # LOAD x- and y-scaler associated to this model
            [scaler_x, scaler_y] = pickle.load(open(scaler, "rb"))

            # prepare the parameters for the model execution for the simulated experiment
            gt_params_h = gt_params.to_numpy()
            if len(env_uncert) == 0: # plain model prediction is used
                gt_params_h = np.append(gt_params_h, [rad, X, 1.89E+21])
            else: # experimental uncertainty for environmental parameters
                gt_params_h = np.append(gt_params_h, [10**np.random.normal(np.log10(rad), env_uncert[0]), 10**np.random.normal(np.log10(X), env_uncert[1]), 1.89E+21])
            gt_params_h = pd.DataFrame(gt_params_h).transpose()
            inpts_log = np.log10(gt_params_h)
            inpts_t = scaler_x.transform(inpts_log)
            pred = NNmodel.predict(inpts_t)
            pred_t = scaler_y.inverse_transform(pred)
            pred_f = 10 ** pred_t

        else: #KM3-Experiment using matlab engine
            gt_params_h = gt_params.to_numpy()
            gt_params_h = np.append(gt_params_h, [rad, X, 1.89E+21])
            gt_params_h = gt_params_h.tolist()
            pred_f = np.asarray(Matlab_Sampling.get_KM3_sample(gt_params_h)[0]).reshape((1,9))
            pred_t = np.log10(pred_f)

        if add_uncert != 0:  # adding uncertainty for experimental result measurement, but not, if full experimental pickle is provided
            pred_t_unc = np.array(
                [np.random.normal(pred_t[0, 0], scale=add_uncert), np.random.normal(pred_t[0, 1], scale=add_uncert),
                 np.random.normal(pred_t[0, 2], scale=add_uncert), np.random.normal(pred_t[0, 3], scale=add_uncert),
                 np.random.normal(pred_t[0, 4], scale=add_uncert), np.random.normal(pred_t[0, 5], scale=add_uncert),
                 np.random.normal(pred_t[0, 6], scale=add_uncert), np.random.normal(pred_t[0, 7], scale=add_uncert),
                 np.random.normal(pred_t[0, 8], scale=add_uncert)])
            pred_f_unc = 10 ** pred_t_unc
            pred_f_unc = np.reshape(pred_f_unc, (1, 9))
            pred_f = pred_f_unc

    else: #experiment from pickled grid -> only works without env_uncert or with specifically prepared uncertainty-grid
        [all_preds_params, all_preds] = pickle.load(open(exp_pickle[0], "rb")) #load specified pickled grid
        [radgrid, Xgrid] = ensMeth.get_grid(gridsize, gridbounds, loggrid) #load current grid

        # compare grid values with pickled params to find correct index of pred
        step = -1
        ok = 0
        for nr, r in enumerate(radgrid):
            for ni, x in enumerate(Xgrid):
                step = step + 1
                if np.isclose(rad, r, rtol=1E-5) and np.isclose(X, x, rtol=1E-5):
                    preds_params = all_preds_params[step]
                    preds = all_preds[step]
                    ok = 1
                    break
            if ok == 1:
                break
        #if ok == 0:
        #    print("ERROR: Selected experiment not found in exp_pickle!")
        #else:
        #    print("Experiment successfully read out from exp_pickle file.")
        pred_f = np.reshape(preds[gt_i], (1, 9))


    if restrict_exp_cond != 0: # if experiment is restricted, only times are allowed that are sensible experiment times (1s to 3d)
        if np.max(pred_f) > 259200 or np.max(pred_f) < 1:
            #print("Experiment rejected due to immeasurable timespan.")
            lockbest = lockbest + 1
            # select new experiment with the so far non-allowed ones locked and recursively call experiment simulation
            rad_X.append(select_experiment(ignore_ES_frame, random_exp, exp_distance, rad_X, savedir, restrict_exp_cond, gridbounds, useprev, lockgrid=lockgrid, lockbest=lockbest))
            return simulate_experiment(gt_i, gt_params, rad_X[-1][0], rad_X[-1][1], model, scaler, add_uncert, restrict_exp_cond, ignore_ES_frame, random_exp, exp_distance, rad_X, savedir, useprev, km_exp, env_uncert, exp_pickle, gridsize, gridbounds, loggrid, lockgrid, lockbest)
        else:
            return pred_f, rad_X[-1]
    else:
        return pred_f, []


def select_experiment(ignore_ES_frame, random_exp, exp_distance, rad_X, savedir, restrict, gridbounds, useprev="", lockgrid=[], lockbest=0, locked=[]):
    "Function to select an experiment based on the current Ensemble Spread or Potential Constraint matrix. Selects maximum until conditions allow current experiment. Sensible conditions for experiments are hard-coded in this function."

    #print("Selecting experiment with largest Ensemble Spread.")
    #print("Top positions locked due to immeasurable timespan: " + str(lockbest))

    Xscale = np.logspace(gridbounds[0][0], gridbounds[0][1], 100)
    Zscale = np.logspace(gridbounds[1][0], gridbounds[1][1], 100)


    if random_exp == 1:
        if restrict == 0:
            return [Xscale[random.randint(0 + ignore_ES_frame, 99-ignore_ES_frame)], Zscale[random.randint(0+ignore_ES_frame, 99-ignore_ES_frame)]]
        else:
            vals_ok = 0
            while vals_ok == 0:
                vals = [Xscale[random.randint(0 + ignore_ES_frame, 99 - ignore_ES_frame)], Zscale[random.randint(0 + ignore_ES_frame, 99 - ignore_ES_frame)]]
                if (5e-6 < vals[0] < 0.01) and (1e12 < vals[1] < 1e16):
                    vals_ok = 1
                else:
                    #print("Rejected values due to experimental boundaries:")
                    #print(vals)
                    pass
            return vals


    if len(useprev) != 0:
        try:
            ens_spread = pd.read_csv(savedir + useprev, index_col=0)
        except:
            ens_spread = pd.read_csv(useprev, index_col=0)
    else:
        try:
            ens_spread = pd.read_csv(savedir + "ensembe_spreads_matrix.csv", index_col=0)
        except:
            ens_spread = pd.read_csv("ensembe_spreads_matrix.csv", index_col=0)



    # this is only executed if no random experiment is selected (otherwise return)
    # apply ignore_ES_frame (no selection from edge of area)
    if ignore_ES_frame != 0:
        ens_spread = ens_spread.iloc[ignore_ES_frame:, :]
        ens_spread = ens_spread.iloc[:-ignore_ES_frame, :]
        ens_spread = ens_spread.iloc[:, ignore_ES_frame:]
        ens_spread = ens_spread.iloc[:, :-ignore_ES_frame]
    if len(lockgrid) == 0:
        max = 0
        maxi = 0
        maxj = 0
        for i, row in ens_spread.iterrows(): # iterate over grid and find maximum until all conditions are satisfied (minimal distance, allowed area)
            for j, val in enumerate(row):
                if val > max:
                    dist_ok = 1
                    if exp_distance != 0 and len(rad_X) != 0: #skip this point if distance to previous experiment is not satisfied
                        x1 = np.log10(i)
                        y1 = np.log10(float(ens_spread.columns[j]))
                        for previ, prevj in rad_X:
                            x2 = np.log10(previ)
                            y2 = np.log10(prevj)
                            dist = np.sqrt((y2 - y1)**2 + (x1 - x2)**2) #log10 distance on plane
                            if dist < exp_distance:
                                dist_ok = 0
                            for lock in locked:
                                if np.isclose(lock, val):
                                    dist_ok = 0
                    rest_ok = 1
                    if restrict != 0:
                        if not ((5e-6 < i < 0.01) and (1e12 < float(ens_spread.columns[j]) < 1e16)):
                            #print("Rejected values due to experimental boundaries:")
                            #print(i)
                            #print(ens_spread.columns[j])
                            rest_ok = 0

                    if dist_ok == 1 and rest_ok == 1: # maximum with all conditions satisfied found
                        max = val
                        maxi = i
                        maxj = float(ens_spread.columns[j])

        if lockbest == 0:
            #print("Experiment selected.")
            return [maxi, maxj]
        else: #recursive call to select next best experiment (best x locked)
            locked.append(max)
            lockbest = lockbest-1
            return select_experiment(ignore_ES_frame, random_exp, exp_distance, rad_X, savedir, restrict, gridbounds, useprev, lockgrid, lockbest, locked)
    else:
        max = 0
        maxi = 0
        maxj = 0
        locks_i = pickle.load(open(lockgrid[0], "rb"))
        ind = 0
        for i, row in ens_spread.iterrows():  # iterate over grid and find maximum until all conditions are satisfied (minimal distance, allowed area)
            for j, val in enumerate(row):
                if locks_i[ind][j] == 0:
                    if val > max:
                        dist_ok = 1
                        if exp_distance != 0 and len(rad_X) != 0:  # skip this point if distance to previous experiment is not satisfied
                            x1 = np.log10(i)
                            y1 = np.log10(float(ens_spread.columns[j]))
                            for previ, prevj in rad_X:
                                x2 = np.log10(previ)
                                y2 = np.log10(prevj)
                                dist = np.sqrt((y2 - y1) ** 2 + (x1 - x2) ** 2)  # log10 distance on plane
                                if dist < exp_distance:
                                    dist_ok = 0

                        if dist_ok == 1:  # maximum with all conditions satisfied found
                            max = val
                            maxi = i
                            maxj = float(ens_spread.columns[j])
            ind = ind+1
        return [maxi, maxj]

def sample_ground_truth(fits, fits_params, fit_labels, usecluster, select_gt_by_indx):
    "Function to select one fit as simulated truth randomly or by specification of index or cluster."

    print("Selecting parameter set from Fit Ensemble as simulated truth.")
    n_fits = fits.shape[0]

    if len(select_gt_by_indx) == 0: #get random
        gt_i = random.randint(0, n_fits - 1)
    else: #or select by provided index
        gt_i = select_gt_by_indx[0]

    if usecluster != -1:  # if specific regime should be used as ground truth
        while fit_labels[gt_i] != usecluster:  # repeat sample until label fits
            gt_i = random.randint(0, n_fits - 1)

    # get ground truth data
    gt_params = fits_params.iloc[gt_i]
    gt_fit = fits.iloc[gt_i]
    gt_label = fit_labels[gt_i]

    return [gt_i, gt_params, gt_fit, gt_label]


def extract_data(file, labelfile=0):
    "Function to load and prepare input files."

    if labelfile == 1:
        return pd.read_csv(file)["label"]

    if labelfile == 0:
        fits = pd.read_csv(file)
        fits = fits.dropna()
        fits = fits.reset_index(drop=True)

        try:
            fits = fits.drop(["rRMSE"], axis=1)
        except:
            pass

        try:
            fits_params = fits.drop(["out1", "out2", "out3"], axis=1)
        except:
            fits_params = fits
        log_fits_params = np.log10(fits_params)

        print("Data loaded.")
        print("Number of Fits: " + str(fits_params.shape[0]))

        return [fits_params, log_fits_params, fits]