# WRAPPERS FOR MULTITUDE OF SIMULATIONS OF ITERATIVE APPLICATION OF THE KINETIC COMPASS
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08

import numpy as np
import Simulation
import os
import csv
from matplotlib import pyplot as plt
from multiprocessing import Pool
import copy

def calibrate_error_th(ens_fits, labeled_ens_fits, fit_iter, sim_iter, exp_iter, model, scaler, n_FE, cmprdata, runfits=[], gridsize=[100, 100], gridbounds=[[-6,-2],[10,16]], loggrid=1, ignore_ES_frame=1, restrict_exp_cond=0, add_uncert=0.08, env_uncert=[0.05, 0.02], exp_distance=0.2, set_random_experiment=1, usepickle=[], makepickle="current.pickle"):
    "This function performs a full calibration of experimental uncertainties for a given number of fits (fit iter, n_FE), iterations (sim_iter) and experiment repetitions (exp_iter). It tracks error development throughout multiple simulated experiments in order to sample uncertainties that lead to similar errors as the real experiments. Provide files that only contain the fits that you want to test for (ens_fits, labeled_ens_fits)."

    if len(runfits) == 0:
        i_runs = range(fit_iter)
    else:
        i_runs = runfits
    if "KM3" in model:
        km_exp = 1
    else:
        km_exp=0
    rad_X = []
    for i in range(sim_iter): #select a number of experiments initially and provide them for all runs to avoid restrictions during function execution
        rad_X.append(Simulation.select_experiment(ignore_ES_frame, set_random_experiment, exp_distance, rad_X, "doesnt matter", restrict_exp_cond, gridbounds))
    exps = range(sim_iter+8)[7:] # from 7 (inclusive, number of real experiments) to 7 + simulation iterations
    all_errors = []
    all_joint_errors = []
    for i in i_runs: #iterate over fits that are tested as ground truth
        errors, error_std, partials = Simulation.perform_simulation(ens_fits, labeled_ens_fits, sim_iter, model, scaler, n_FE, cmprdata, gridsize=gridsize, gridbounds=gridbounds, loggrid=loggrid, remove_ground_truth=0, usecluster=-1, ignore_ES_frame=ignore_ES_frame, restrict_exp_cond=restrict_exp_cond, add_uncert=add_uncert, env_uncert=env_uncert, set_random_experiment=set_random_experiment, exp_distance=exp_distance, select_gt_by_indx=[i], km_exp=km_exp, error_th_calib=1, exp_iter=exp_iter, external_exp=rad_X, usepickle=usepickle, makepickle=makepickle)
        all_errors.append(errors) #average partial errors of each iteration, first one is for 7 original experiments
        joint_errors = []
        #join errors for each round considering the previous average partial error (joint error of all experiments up to this point in the simulation!)
        for p1, partial in enumerate(partials):
            subjoint_errors = []
            for p2, part in enumerate(partial):
                jointerr = 7*errors[0] # weight the original average error *7 as it originates from 7 experiments
                for s in range(p1):
                    jointerr = jointerr + errors[s+1] # add the average partial error of all previous simulated experiments with weight 1
                jointerr = jointerr + part # and the individual partial error of the experiment repetition is considered with weight 1
                subjoint_errors.append(jointerr/(8+p1)) # now divide by 7 (original exp) + 1 (current partial) + p1 (number of previous average partials)
            joint_errors.append(subjoint_errors) # this is the error that we consider joint error of all experiments at this point for each of the partials from repetitions
        all_joint_errors.append(joint_errors)

    plot_error_calib(all_errors, all_joint_errors, exps, env_uncert, add_uncert)

    return

def plot_error_calib(errors, all_joint_errors, exps, env_unc, t_unc):
    "Plotting function for uncertainty calibration."

    for i, joint in enumerate(all_joint_errors):
        joint_avgs = [errors[i][0]] #add original error of all experiments for first data point
        joint_stds = [0] #... with std = 0 (no repetitions)
        for j, exp_ens in enumerate(joint):
            joint_avgs.append(np.average((exp_ens)))
            joint_stds.append(np.std(exp_ens))
        plt.errorbar(exps, joint_avgs, joint_stds, capsize=5)
    plt.xlabel("Experiments")
    plt.ylabel("MSLE")
    plt.savefig("error_calibration_t_" + str(t_unc)[2:] + "_r_" + str(env_unc[0])[2:] + "_x_" + str(env_unc[1])[2:] + ".pdf")
    plt.show()

    return


def iterate_one_fit(ens_fits, labeled_ens_fits, iter, model, scaler, n_FE, runs, runname, runfits, cmprdata, gridsize=[100, 100], gridbounds=[[-6,-2],[10,16]], loggrid=1, ignore_ES_frame=0, restrict_exp_cond=1, add_uncert=0, filter_th=1.3, exp_distance=0.2, smooth_interp=1, relative_fit_filtering=0, km_exp=1, plotwith=[], usepickle=[], makepickle="current.pickle", exp_pickle=[], specified_filter_pickle=[], lockgrid=[]):
    "Function to repeat simulations with experiments that are rendomly selected without application of the Ensemble Method - allows benchmarking by estimating average unsupported model refinement."

    set_random_experiment=1
    for i in runfits: #iterate over specified fits
        plot_vals = []
        for j in range(runs): #repeat simulation as many times as desired with different randomly selected experiments
            savedir, endinfo, endvals = Simulation.perform_simulation(ens_fits, labeled_ens_fits, iter, model, scaler, n_FE, cmprdata, gridsize=gridsize, gridbounds=gridbounds, loggrid=loggrid, remove_ground_truth=0, usecluster=-1, ignore_ES_frame=ignore_ES_frame, restrict_exp_cond=restrict_exp_cond, add_uncert=add_uncert, filter_th=filter_th, set_random_experiment=set_random_experiment, exp_distance=exp_distance, select_gt_by_indx=[i], smooth_interp=smooth_interp, relative_fit_filtering=relative_fit_filtering, km_exp=km_exp, usepickle=usepickle, makepickle=makepickle, exp_pickle=exp_pickle, specified_filter_pickle=specified_filter_pickle, lockgrid=lockgrid)
            plot_vals.append(endvals[2])
            os.rename(savedir, runname + "_" + str(i) + "_" + str(j))
            if i == 0 and j == 0: # initialize file for summary
                file = open(runname + "_info.csv", "w")
                writer=csv.writer(file)
                writer.writerow(endinfo)
                writer.writerow(endvals)
                file.close()
            else:
                with open(runname + "_info.csv", "a+") as write_obj: #append to summary file
                    writer=csv.writer(write_obj)
                    writer.writerow(endvals)
        for j, plotval in enumerate(plot_vals): #make basic plot for visualization
            plt.plot(range(len(plotval)), plotval, color="b", label="Random System Parameter Selection")
        if len(plotwith) != 0:
            plt.plot(range(len(plotwith[i])), plotwith[i], color="r", linewidth=3, label="Ensemble Method")
        plt.ylabel("Number of accepted fits")
        plt.xlabel("Simulation iteration")
        plt.legend()
        plt.show()
    return

def iterate_fits(ens_fits, labeled_ens_fits, iter, model, scaler, n_FE, runs, runname, cmprdata, subcluster_meth=[],  subcluster_bin_size=0, subcluster_error_bin=0, continuous_density=20, continuous_bins=0.05, boundnorm=1, runfits=[], gridsize=[100, 100], gridbounds=[[-6,-2],[10,16]], loggrid=1, remove_ground_truth=0, usecluster=-1, ignore_ES_frame=0, restrict_exp_cond=1, add_uncert=0, env_uncert=[], filter_th=0.013, set_random_experiment=0, useprevious="", exp_distance=0.2, smooth_interp=1, relative_fit_filtering=0, filter_by_exchange=0, filter_by_gt_calibration=0, km_exp=1, usepickle=[], makepickle="current.pickle", exp_pickle=[], specified_filter_pickle=[], lockgrid=[]):
    "Standard function to perform simulations for multiple fits in the ensemble and save the results in unclattered folder structure including info."

    if len(runfits) == 0: # if not provided, start from beginning and run as many fits as specified in runs
        i_runs = range(runs)
    else:
        i_runs = runfits
    for i in i_runs: #iterate over fits and perform simulation as specified
        savedir, endinfo, endvals = Simulation.perform_simulation(ens_fits, labeled_ens_fits, iter, model, scaler, n_FE, cmprdata, subcluster_meth=subcluster_meth,  subcluster_bin_size=subcluster_bin_size, subcluster_error_bin=subcluster_error_bin, continuous_density=continuous_density, continuous_bins=continuous_bins, gridsize=gridsize, gridbounds=gridbounds, loggrid=loggrid, remove_ground_truth=remove_ground_truth, usecluster=usecluster, ignore_ES_frame=ignore_ES_frame, restrict_exp_cond=restrict_exp_cond, add_uncert=add_uncert, env_uncert=env_uncert, filter_th=filter_th, set_random_experiment=set_random_experiment, useprevious=useprevious, exp_distance=exp_distance, select_gt_by_indx=[i], smooth_interp=smooth_interp, relative_fit_filtering=relative_fit_filtering, filter_by_exchange=filter_by_exchange, filter_by_gt_calibration=filter_by_gt_calibration, km_exp=km_exp, usepickle=usepickle, makepickle=makepickle, exp_pickle=exp_pickle, specified_filter_pickle=specified_filter_pickle, lockgrid=lockgrid)
        os.rename(savedir, runname + "_" + str(i))
        if i == 0: #initialze info file
            file = open(runname + "_info.csv", "w")
            writer=csv.writer(file)
            writer.writerow(endinfo)
            writer.writerow(endvals)
            file.close()
        else: #append to info file
            with open(runname + "_info.csv", "a+") as write_obj:
                writer=csv.writer(write_obj)
                writer.writerow(endvals)
    return

def iterate_fits_parallel(ens_fits, labeled_ens_fits, iter, model, scaler, n_FE, runs, runname, cmprdata, subcluster_meth=[],  subcluster_bin_size=0, subcluster_error_bin=0, continuous_density=20, continuous_bins=0.05, boundnorm=1, runfits=[], gridsize=[100, 100], gridbounds=[[-6,-2],[10,16]], loggrid=1, remove_ground_truth=0, usecluster=-1, ignore_ES_frame=0, restrict_exp_cond=1, add_uncert=0, env_uncert=[], filter_th=0.013, set_random_experiment=0, useprevious="", exp_distance=0.2, smooth_interp=1, relative_fit_filtering=0, filter_by_exchange=0, filter_by_gt_calibration=0, km_exp=1, usepickle=[], makepickle="current.pickle", exp_pickle=[], specified_filter_pickle=[], lockgrid=[]):
    "Standard function to perform simulations for multiple fits in the ensemble and save the results in unclattered folder structure including info."

    inp = [ens_fits, labeled_ens_fits, iter, model, scaler, n_FE, cmprdata, subcluster_meth,  subcluster_bin_size, subcluster_error_bin, continuous_density, continuous_bins, gridsize, gridbounds, loggrid, remove_ground_truth, usecluster, ignore_ES_frame, restrict_exp_cond, add_uncert, env_uncert, filter_th, set_random_experiment, useprevious, exp_distance, smooth_interp, relative_fit_filtering, filter_by_exchange, filter_by_gt_calibration, km_exp, usepickle, makepickle, exp_pickle, specified_filter_pickle, lockgrid]

    if len(runfits) == 0: # if not provided, start from beginning and run as many fits as specified in runs
        i_runs = range(runs)
    else:
        i_runs = runfits

    procs = len(i_runs)
    inputs = []
    for i in i_runs:
        inputs.append(copy.deepcopy(inp))
        inputs[-1].append(i)

    print("Begin multiprocessing")

    with Pool(len(i_runs)) as p:
        returns = p.map(pool_jobs, inputs)


    for i, [savedir, endinfo, endvals] in enumerate(returns): #iterate over fits and perform simulation as specified
        os.rename(savedir, runname + "_" + str(i))
        if i == 0: #initialze info file
            file = open(runname + "_info.csv", "w")
            writer=csv.writer(file)
            writer.writerow(endinfo)
            writer.writerow(endvals)
            file.close()
        else: #append to info file
            with open(runname + "_info.csv", "a+") as write_obj:
                writer=csv.writer(write_obj)
                writer.writerow(endvals)
    return

def pool_jobs(inputs):

    [ens_fits, labeled_ens_fits, iter, model, scaler, n_FE, cmprdata, subcluster_meth, subcluster_bin_size,
     subcluster_error_bin, continuous_density, continuous_bins, gridsize, gridbounds, loggrid, remove_ground_truth, usecluster,
     ignore_ES_frame, restrict_exp_cond, add_uncert, env_uncert, filter_th, set_random_experiment, useprevious,
     exp_distance, smooth_interp, relative_fit_filtering, filter_by_exchange, filter_by_gt_calibration, km_exp,
     usepickle, makepickle, exp_pickle, specified_filter_pickle, lockgrid, i] = inputs

    return Simulation.perform_simulation(ens_fits, labeled_ens_fits, iter, model, scaler, n_FE, cmprdata, subcluster_meth=subcluster_meth,  subcluster_bin_size=subcluster_bin_size, subcluster_error_bin=subcluster_error_bin, continuous_density=continuous_density, continuous_bins=continuous_bins, boundnorm=boundnorm, gridsize=gridsize, gridbounds=gridbounds, loggrid=loggrid, remove_ground_truth=remove_ground_truth, usecluster=usecluster, ignore_ES_frame=ignore_ES_frame, restrict_exp_cond=restrict_exp_cond, add_uncert=add_uncert, env_uncert=env_uncert, filter_th=filter_th, set_random_experiment=set_random_experiment, useprevious=useprevious, exp_distance=exp_distance, select_gt_by_indx=[i], smooth_interp=smooth_interp, relative_fit_filtering=relative_fit_filtering, filter_by_exchange=filter_by_exchange, filter_by_gt_calibration=filter_by_gt_calibration, km_exp=km_exp, usepickle=usepickle, makepickle=makepickle, exp_pickle=exp_pickle, specified_filter_pickle=specified_filter_pickle, lockgrid=lockgrid)