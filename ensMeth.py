# KINETIC COMPASS FRAMEWORK INCLUDING ENSEMBLE SPREAD AND PARAMETER CONSTRAINT POTENTIAL METRICS
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08


import random
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import pickle
import csv
import scipy
from tensorflow import keras
from scipy.interpolate import interp1d
from scipy import integrate
import seaborn as sns
import Matlab_Sampling
import Simulation
import error_th_calibration
import plotting
import copy


def perform_ensemble_meth(fitfile, n_fits, gridsize, gridbounds, model, scaler, loggrid=0, subcluster_meth=[], subcluster_bin_size=5, subcluster_error_bin=0.011, continuous_density=1,  continuous_bins=0.05, labelsf=[], interp_dens=0.01, both=1, fulldata=[], savedir="", use_pickle=[], make_pickle="current.pickle", smooth_interp=1, make_only_param_pickle=0, x_interp=[], set_envs=[], envorder=[], plot=0, plot_3d_th=2, showzeros=0, plotfrompickle=[], fitplot_num=5, boundnorm=1):
    "Function to perform full Ensemble Method or Subcluster Method, resulting in a map for two environmental parameters that indicates experiments that may allow an optimal refinement of the model (reduction of fits) or constraint of non-environmental input parameters. Providing param names as strings in subcluster_meth=[] will switch off Ensemble Method."

    if len(subcluster_meth) == 0:
        print("Performing ensemble method to determine Ensemble and Cluster Spread.")
    else:
        print("Performing ensemble method to determine Subcluster Constraint Potential.")
    if len(use_pickle) == 0 and "km" not in model:
        # LOAD NN FORWARD MODEL (3 outputs)
        NNmodel = keras.models.load_model(model)

        # LOAD x- and y-scaler associated to this model
        [scaler_x, scaler_y] = pickle.load(open(scaler, "rb"))

    #extract n_fits parameter sets from fitfile
    if len(fulldata) == 0:
        fits = extract_fits(fitfile, n_fits)
    else:
        fits = fulldata[0][:n_fits]

    if len(gridsize) == 2:
        [env1, env2] = get_grid(gridsize, gridbounds, loggrid)

    if len(gridsize) == 3:
        #get grid in log-space
        [env1, env2, env3] = get_grid(gridsize, gridbounds, loggrid)

        if len(plotfrompickle) != 0: #plotfrompickle only implemented for 3D plots
            print("No Ensemble Method evaluation, plotting from supplied result pickle...")
            [heatmap, predmap, fitplotdata, expplotdata] = pickle.load(open(plotfrompickle[0], "rb"))
            plot_3d_ES(env1, env2, env3, heatmap, predmap, showzeros, plot_3d_th, gridsize, fitplotdata, expplotdata, plotfrompickle)
            #plot_3d_ES(env1, env2, env3, heatmap, showzeros, plot_3d_th, gridsize, fitplotdata, invert=1)
            return 1

    #rad = [1.00E-03, 2.00E-05, 0.00004, 2.50E-05, 2.50E-05, 2.50E-05]
    #io3 = [9970000000000, 70000000000000, 2.50E+15, 2.00E+14, 3.25E+14, 5.51E+14]

    # get labels
    labels = []
    if len(fulldata) == 0:
        if len(labelsf) != 0:
            labels = extract_fits(labelsf[0], n_fits, labels=1)
    else:
        labels = fulldata[1][:n_fits]

    # loop over grid and save ensemble data
    inpts1 = []
    inpts2 = []
    inpts3 = []
    e_spread = []
    fitplotdata = []
    expplotdata = []
    saved_preds = []
    #pltexp = [[5750, 1583, 1], [5750, 1673, 1], [5750, 1763, 1]]
    step = 0
    if len(gridsize) == 3:
        predmap = np.zeros(((gridsize[0], gridsize[1], gridsize[2], n_fits)))
        heatmap = np.zeros((gridsize[0], gridsize[1], gridsize[2]))
        dist = np.zeros((gridsize[0], gridsize[1], gridsize[2]))
    elif len(gridsize) == 2:
        predmap = np.zeros(((gridsize[0], gridsize[1], n_fits)))
        heatmap = np.zeros((gridsize[0], gridsize[1]))
        dist = np.zeros((gridsize[0], gridsize[1]))
    if len(use_pickle) == 0:
        all_preds_params = []
        all_preds = []
    else:
        [all_preds_params, all_preds] = pickle.load(open(use_pickle[0], "rb"))


    for ne1, e1 in enumerate(env1):
        for ne2, e2 in enumerate(env2):
            if len(gridsize) > 2:

                for ne3, e3 in enumerate(env3):

                    #progress
                    step = step + 1
                    if step%100 == 0:
                        print("Step " + str(step) + " of " + str(len(env1) * len(env2) * len(env3)))

                    # set all environmental parameters for this cell including fixed ones
                    setenvs = []
                    for i_senv, senv in enumerate(set_envs):
                        setenvs.append([senv for x in range(n_fits)])
                    mutenvs = [[e1 for x in range(n_fits)], [e2 for x in range(n_fits)], [e3 for x in range(n_fits)]]

                    for envnam in envorder:
                        if "mutenv" in envnam:
                            fits[envnam] = mutenvs[int(envnam[-1])]
                        elif "senv" in envnam:
                            fits[envnam] = setenvs[int(envnam[-1])]
                    # get predictions for current ensemble
                    #use model to make predictions and save all parameters and predictions for pickle
                    if len(use_pickle) == 0:
                        if make_only_param_pickle == 1: #skip predictions to get quick param pickle
                            all_preds_params.append(fits.copy())
                            all_preds.append([])
                            continue

                        else:
                            if not "km" in model: #NN
                                preds = nn_model_pred(fits, NNmodel, scaler_x, scaler_y)
                                all_preds.append(preds)
                                all_preds_params.append(fits.copy())
                            else: #Matlab:kinetic model
                                preds = []
                                for row in fits.iterrows():
                                    preds.append(Matlab_Sampling.get_KM3_sample(list(row[1]), sim=0))
                                all_preds.append(preds)
                                all_preds_params.append(fits.copy())
                    else:
                        # get pickled data from current step
                        unfiltered_preds = all_preds[step-1]
                        unfiltered_fits = all_preds_params[step-1]

                        # merge fit dataframes and obtain indices of duplicates to get params
                        for fi in range(len(unfiltered_fits.columns)): #make sure that columns are equally named
                            unfiltered_fits = unfiltered_fits.rename(columns={unfiltered_fits.columns[fi]: fits.columns[fi]})

                        #merge and find duplicates to get index for associated predictions
                        merged = pd.concat([unfiltered_fits, fits])
                        dupls = merged.duplicated(keep='last')
                        dupls_l = dupls[dupls].index

                        try:
                            preds = unfiltered_preds[dupls_l, :] #collect preds based on duplicate values in merged dataframe
                        except:
                            try:
                                preds = np.array(unfiltered_preds)[dupls_l, :] # in case, unfiltered_preds is passed as list
                            except:
                                preds = np.array(unfiltered_preds).reshape((len(unfiltered_preds), 1))
                    if len(subcluster_meth) != 0: #make subclusters/bins and quantify their distance to find experiment associated with best constraint
                        e_spread.append(subcluster_method(subcluster_meth, subcluster_bin_size, subcluster_error_bin, continuous_density, continuous_bins, preds, n_fits, fits, sample_plot=0))
                        heatmap[ne1][ne2][ne3] = e_spread[-1]


                    # get values from predictions
                    elif both==1 or len(labels) == 0:
                        e_spread.append(handle_preds(preds, interp_dens, x_interp, smooth=smooth_interp))
                        heatmap[ne1][ne2][ne3] = e_spread[-1]

                    # if lables are given, also handle clusters
                    if len(labels) != 0:
                        try:
                            dist[ne1][ne2][ne3] = handle_preds(preds, interp_dens, x_interp, labels=labels, smooth=smooth_interp)
                        except:
                            pass
                    inpts1.append(e1)
                    inpts2.append(e2)
                    inpts3.append(e3)
                    saved_preds.append(preds)

                    #if fitplot_num != 0:
                    #    for exps in pltexp:
                    #        if e1 == exps[0] and e2 == exps[1] and e3 == exps[2]:
                    #            expplotdata.append(["log10(ES): " + str(np.round(np.log10(e_spread[-1]), 3)) + "; [H2O]: " + str(np.round(e1, 1)) + "; \n T [K]: " + str(np.round(e2, 1)) + ", r_t [s]: " + str(np.round(e3, 1)), preds])

                    # collect data for detailed prediction-plots
                    for npr, pr in enumerate(preds):
                        predmap[ne1][ne2][ne3][npr] = pr

            else:
                # progress
                step = step + 1
                if step % 100 == 0:
                    print("Step " + str(step) + " of " + str(len(env1) * len(env2)))

                # set all environmental parameters for this cell including fixed ones
                setenvs = []
                for i_senv, senv in enumerate(set_envs):
                    setenvs.append([senv for x in range(n_fits)])
                mutenvs = [[e1 for x in range(n_fits)], [e2 for x in range(n_fits)]]

                for envnam in envorder:
                    if "mutenv" in envnam:
                        fits[envnam] = mutenvs[int(envnam[-1])]
                    elif "senv" in envnam:
                        fits[envnam] = setenvs[int(envnam[-1])]
                # get predictions for current ensemble
                # use model to make predictions and save all parameters and predictions for pickle
                if len(use_pickle) == 0:
                    if make_only_param_pickle == 1:  # skip predictions to get quick param pickle
                        all_preds_params.append(fits.copy())
                        all_preds.append([])
                        continue

                    else:
                        if not "km" in model:  # NN
                            preds = nn_model_pred(fits, NNmodel, scaler_x, scaler_y)
                            all_preds.append(preds)
                            all_preds_params.append(fits.copy())
                        else:  # Matlab:kinetic model
                            preds = []
                            for row in fits.iterrows():
                                preds.append(Matlab_Sampling.get_KM3_sample(list(row[1]), sim=0))
                            all_preds.append(preds)
                            all_preds_params.append(fits.copy())
                else:
                    # get pickled data from current step
                    unfiltered_preds = all_preds[step - 1]
                    unfiltered_fits = all_preds_params[step - 1]

                    # merge fit dataframes and obtain indices of duplicates to get params
                    for fi in range(len(unfiltered_fits.columns)):  # make sure that columns are equally named
                        unfiltered_fits = unfiltered_fits.rename(columns={unfiltered_fits.columns[fi]: fits.columns[fi]})

                    # merge and find duplicates to get index for associated predictions
                    merged = pd.concat([unfiltered_fits, fits])
                    dupls = merged.duplicated(keep='last')
                    dupls_l = dupls[dupls].index

                    try:
                        preds = unfiltered_preds[dupls_l, :]  # collect preds based on duplicate values in merged dataframe
                    except:
                        try:
                            preds = np.array(unfiltered_preds)[dupls_l, :]  # in case, unfiltered_preds is passed as list
                        except:
                            preds = np.array(unfiltered_preds).reshape((len(unfiltered_preds), 1))

                if preds.shape[0] == 0:
                    return 0

                if len(subcluster_meth) != 0:  # make subclusters/bins and quantify their distance to find experiment associated with best constraint
                    e_spread.append(subcluster_method(subcluster_meth, subcluster_bin_size, subcluster_error_bin,
                                                      continuous_density, continuous_bins, preds, n_fits, fits,
                                                      sample_plot=0))
                    heatmap[ne1][ne2] = e_spread[-1]


                # get values from predictions
                elif both == 1 or len(labels) == 0:
                    e_spread.append(handle_preds(preds, interp_dens, x_interp, smooth=smooth_interp))
                    heatmap[ne1][ne2] = e_spread[-1]

                # if lables are given, also handle clusters
                if len(labels) != 0:
                    try:
                        dist[ne1][ne2] = handle_preds(preds, interp_dens, x_interp, labels=labels, smooth=smooth_interp)
                    except:
                        pass
                inpts1.append(e1)
                inpts2.append(e2)
                saved_preds.append(preds)

    if len(gridsize) == 3:
        if len(subcluster_meth) != 0 and boundnorm != 0:
            #minval = np.min(heatmap[np.nonzero(heatmap)])
            minval = 1000000
            for i in range(heatmap.shape[0]):
                for j in range(heatmap.shape[1]):
                    for k in range(heatmap.shape[2]):
                        if heatmap[i][j][k] > 0 and heatmap[i][j][k] < minval:
                            minval = copy.deepcopy(heatmap[i][j][k])
            for i in range(heatmap.shape[0]):
                for j in range(heatmap.shape[1]):
                    for k in range(heatmap.shape[2]):
                        if heatmap[i][j][k] > 0:
                            heatmap[i][j][k] = (heatmap[i][j][k] - minval)/boundnorm
    if len(gridsize) == 2:
        if len(subcluster_meth) != 0 and boundnorm != 0:
            #minval = np.min(heatmap[np.nonzero(heatmap)])
            minval = 1000000
            for i in range(heatmap.shape[0]):
                for j in range(heatmap.shape[1]):
                    if heatmap[i][j] > 0 and heatmap[i][j] < minval:
                        minval = copy.deepcopy(heatmap[i][j])
            for i in range(heatmap.shape[0]):
                for j in range(heatmap.shape[1]):
                    if heatmap[i][j] > 0:
                        heatmap[i][j] = (heatmap[i][j] - minval)/boundnorm

    #if fitplot_num != 0:
    #    res = sorted(range(len(e_spread)), key=lambda sub: e_spread[sub])[-fitplot_num:]
    #    for r in res:
    #        fitplotdata.append(["log10(ES): " + str(np.round(np.log10(e_spread[-1]), 3)) + "; [H2O]: " + str(
    #            np.round(inpts1[r], 1)) + "; \n T [K]: " + str(np.round(inpts2[r], 1)) + "; r_t [s]: " + str(np.round(inpts3[r], 1)), saved_preds[r]])

    if make_only_param_pickle == 1:
        pickle.dump([all_preds_params, all_preds], open(make_pickle, "wb"))
        return 1
    #save data to csv and make plot
    if len(gridsize) == 3:
        if both == 1 or len(labels) == 0:
            cols = pd.MultiIndex.from_product([env2, env3])
            returnmatrix = pd.DataFrame(data=heatmap.reshape(gridsize[0], -1), index=env1, columns=cols)
            if len(subcluster_meth) == 0:
                returnmatrix.to_csv(savedir + "ensembe_spreads_matrix.csv")
            else:
                returnmatrix.to_csv(savedir + "subcluster_meth_" + "".join([x for x in subcluster_meth]) + "_matrix.csv")
            returndf = pd.DataFrame()
            returndf["env1"] = inpts1
            returndf["env2"] = inpts2
            returndf["env3"] = inpts3
            returndf["ens_spread"] = e_spread

            returndf = returndf.sort_values(by="ens_spread", ascending=False)

            if len(subcluster_meth) == 0:
                returndf.to_csv(savedir + "ensemble_spreads.csv")
                pickle.dump([heatmap, predmap, fitplotdata, expplotdata], open("current_ens_spread_heatmap.pickle", "wb"))
            else:
                returndf.to_csv(savedir + "subcluster_meth_" + "".join([x for x in subcluster_meth]) + ".csv")
                pickle.dump([heatmap, predmap, fitplotdata, expplotdata], open("current_const_pot" + "".join([x for x in subcluster_meth]) + "_heatmap.pickle", "wb"))


        if len(labels) != 0:
            try:
                returnmatrix2 = pd.DataFrame(data=dist, index=env1, columns=env2)
                returnmatrix2.to_csv(savedir + "ensembe_spreads_cluster_mean_diff_matrix.csv")

                #col = sns.color_palette("coolwarm")
                #sns.heatmap(dist, annot=True, cmap=col)
                #plt.xlabel("X(0)")
                #plt.ylabel("rad")
                #plt.show()
            except:
                pass
    if len(gridsize) == 2:
        if both == 1 or len(labels) == 0:
            returnmatrix = pd.DataFrame(data=heatmap, index=env1, columns=env2)
            returnmatrix.to_csv(savedir + "ensembe_spreads_matrix.csv")

            returndf = pd.DataFrame()
            returndf["rad"] = inpts1
            returndf["X0"] = inpts2
            returndf["ens_spread"] = e_spread

            returndf = returndf.sort_values(by="ens_spread", ascending=False)
            returndf.to_csv(savedir + "ensemble_spreads.csv")

            #col = sns.color_palette("coolwarm")
            #sns.heatmap(heatmap, annot=True, cmap=col)
            #plt.xlabel("X(0)")
            #plt.ylabel("rad")
            #plt.show()

        if len(labels) != 0:
            try:
                returnmatrix2 = pd.DataFrame(data=dist, index=env1, columns=env2)
                returnmatrix2.to_csv(savedir + "ensembe_spreads_cluster_mean_diff_matrix.csv")

                #col = sns.color_palette("coolwarm")
                #sns.heatmap(dist, annot=True, cmap=col)
                #plt.xlabel("X(0)")
                #plt.ylabel("rad")
                #plt.show()
            except:
                pass

    # after successful run, pickle everything for the future if no pickle was provided
    if len(use_pickle) == 0:
        pickle.dump([all_preds_params, all_preds], open(make_pickle, "wb"))

    if (plot == 1) and (len(gridsize) == 3): #3D plotting
        plot_3d_ES(env1, env2, env3, heatmap, predmap, showzeros, plot_3d_th, gridsize, fitplotdata, expplotdata, plotfrompickle)

    return 1


def plot_3d_ES(env1, env2, env3, heatmap, predmap, showzeros, plot_3d_th, gridsize, fitplotdata, expplotdata, savename, invert=0, log=1, log1=0):
    if len(savename) == 0:
        savenamestr = "current_run"
    else:
        savenamestr = savename[0]

    ax = plt.axes(projection='3d')
    xline = []
    yline = []
    zline = []
    cline = []

    for ne1, e1 in enumerate(env1):
        for ne2, e2 in enumerate(env2):
            for ne3, e3 in enumerate(env3):
                if log == 1:
                    if np.log10(heatmap[ne1][ne2][ne3]) > plot_3d_th:
                        xline.append(e1)
                        yline.append(e2)
                        zline.append(e3)
                        cline.append(np.log10(heatmap[ne1][ne2][ne3]))
                else:
                    if heatmap[ne1][ne2][ne3] > plot_3d_th:
                        xline.append(e1)
                        yline.append(e2)
                        zline.append(e3)
                        cline.append(heatmap[ne1][ne2][ne3])
    if showzeros == 1:
        xzeros = []
        yzeros = []
        zzeros = []
        for ne1, e1 in enumerate(env1):
            for ne2, e2 in enumerate(env2):
                for ne3, e3 in enumerate(env3):
                    if heatmap[ne1][ne2][ne3] == 0:
                        xzeros.append(e1)
                        yzeros.append(e2)
                        zzeros.append(e3)
        ax.scatter3D(xzeros, yzeros, zzeros, c="gray")

    # Data for a three-dimensional line
    # zline = np.linspace(gridbounds[0][0], gridbounds[0][1], gridsize[0])
    ax.scatter3D(xline, yline, zline, c=cline, cmap="Reds")
    #plt.colorbar(mappable="Reds", vmax=max(cline), vmin=plot_3d_th)
    if invert == 1:
        plt.gca().invert_xaxis()
        plt.gca().invert_yaxis()
    plt.savefig(savenamestr[:-7] + "_cube_" + str(plot_3d_th) + ".png")
    #plt.show()
    plt.close()

    #fitplot
    #preddata = np.array([fitplotdata[x][1] for x in range(len(fitplotdata))]).reshape((len(fitplotdata), len(fitplotdata[0][1])))
    #plotdf = pd.DataFrame(preddata).transpose()
    #plotdf.boxplot()
    #plt.xticks(range(1, len(fitplotdata)+1), [x[0] for x in fitplotdata])
    #plt.xticks(rotation=45, ha="right")
    #plt.ylabel("Mass at r_t")
    #plt.ylim(0, 2E-10)
    #plt.tight_layout()
    #plt.show()
    #expdata = np.array([expplotdata[x][1] for x in range(len(expplotdata))]).reshape(
    #    (len(expplotdata), len(expplotdata[0][1])))
    #plotdf2 = pd.DataFrame(expdata).transpose()
    #plotdf2.boxplot()
    #plt.xticks(range(1, len(expplotdata)+1), [x[0] for x in expplotdata])
    #plt.xticks(rotation=45, ha="right")
    #plt.ylabel("Mass at r_t")
    #plt.ylim(0, 2E-10)
    #plt.tight_layout()
    ##plt.show()
    #plt.close()

    #fitplot2
    groups = []
    env_groups = []
    for rti, rtim in [[0,1], [1,2.45], [2,3.9], [3,5.35], [4,6.8]]:
        subgroups = []
        env_subgroups = []
        for ti, temp in [[7, 1583], [10, 1673], [13, 1763]]:
            subgroup = []
            for ipr in range(predmap.shape[3]):
                subgroup.append(predmap[0:7,ti,rti,ipr])
            print(heatmap[5][ti][rti])
            env_subgroups.append([temp, rtim, [1000, 1950, 2900, 3850, 4800, 5750, 6700]])
            subgroups.append(subgroup)
        groups.append(subgroups)
        env_groups.append(env_subgroups)

    colmap = ["g", "b", "y", "r"]
    for pli, plotdata in enumerate(groups):
        for eni, ensembledata in enumerate(plotdata):
            for fii, fit in enumerate(ensembledata):
                if fii == 0:
                    plt.plot(env_groups[pli][eni][2], fit, c=colmap[eni], linewidth=0.5, label=str(env_groups[pli][eni][0]) + " K")
                else:
                    plt.plot(env_groups[pli][eni][2], fit, c=colmap[eni], linewidth=0.5)
        plt.legend()
        plt.xlabel("[H2O]ini [ppb]")
        plt.ylabel("Particle mass [g]")
        plt.ylim(0, 2.2E-10)
        plt.show()
        plt.close()

    # cut plots
    if log1 != 0:
        levels = np.linspace(-20, -10, 40)
    else:
        levels = np.linspace(0, np.max(heatmap), 40)
    for dim in range(3):
        if dim == 0:
            hm = heatmap.shape[0]
            cut = env1
            var1 = env2
            var2 = env3
            xl = "Temperature [K]"
            yl = "Residence Time [s]"
            xg = gridsize[1]
            yg = gridsize[2]
        elif dim == 1:
            hm = heatmap.shape[1]
            var1 = env1
            cut = env2
            var2 = env3
            xl = "[H20]"
            yl = "Residence Time [s]"
            xg = gridsize[0]
            yg = gridsize[2]
        elif dim == 2:
            hm = heatmap.shape[2]
            var1 = env1
            var2 = env2
            cut = env3
            xl = "[H20]"
            yl = "Temperature [K]"
            xg = gridsize[0]
            yg = gridsize[1]
        for i in range(hm):
            if dim == 0:
                plane = pd.DataFrame(heatmap[i])
            elif dim == 1:
                plane = pd.DataFrame(heatmap[:, i, :])
            elif dim == 2:
                plane = pd.DataFrame(heatmap[:, :, i])
            if log1 != 0:
                plane = np.log10(plane)
            plt.contourf(plane, levels=levels, cmap='coolwarm', fontsize=12)
            plt.yticks([x for n, x in enumerate([x * ((xg - 1) / (len(var1) - 1)) for x in range(len(var1))]) if n%2 == 0], [np.round(x,1) for n, x in enumerate(var1) if n%2 == 0], fontsize=12)
            plt.xticks([x for n, x in enumerate([x * ((yg - 1) / (len(var2) - 1)) for x in range(len(var2))]) if n%2 == 0], [np.round(x,1) for n, x in enumerate(var2) if n%2 == 0], fontsize=12)
            plt.xticks(rotation=45, ha="right")
            plt.ylabel(xl, fontsize=14)
            plt.xlabel(yl, fontsize=14)
            plt.tight_layout()
            cbar = plt.colorbar()
            if log1 != 0:
                cbar.set_label("log10 of Ensemble Spread", fontsize=14)
            else:
                cbar.set_label("Ensemble Spread", fontsize=14)
            #plt.savefig("ES_cut_" + str(dim) + "_" + str(np.round(cut[i], 1))[:-2] + str(np.round(cut[i], 1))[-1] + ".png")
            #plt.show()
            plt.close()
    return


def subcluster_method(subcluster_meth, subcluster_bin_size, subcluster_error_bin, continuous_density, continuous_bins, preds, n_fits, fits, sample_plot=0, sortbyout=4, log=1):
    "Function to call Subcluster method instead of Ensemble method to suggest experiments that may allow a maximal constraint of one or multiple non-environmental parameters. Different Methods provided, see Simulation input parameter documentation. continuous_bins >(overrules) subcluster_error_bin > subcluster_bin_size."

    params = [] #get the kinetic parameter values from fits
    for parnum in range(len(subcluster_meth)):  # specified kinetic parameters
        param = []
        for fitnum in range(n_fits):
            param.append(fits[subcluster_meth[parnum]][fitnum])
        params.append(param)
    potential_constraints = []

    if continuous_bins == 0:  # binning/clustering of fits
        for parnum, param in enumerate(params):
            if subcluster_error_bin == 0: #use a fixed size of bins
                outs = [x[4] for x in preds]  # model predictions
                # make bins based on min and max of outputs and desired number of bins
                min = np.log10(np.min(outs))
                max = np.log10(np.max(outs))
                bins = []
                n_bins = 0
                current = min
                while current < max:
                    n_bins = n_bins+1
                    bins.append(min+subcluster_bin_size*n_bins) #top out value of each bin
                    current = min+subcluster_bin_size*n_bins

                sorted_params = [x for _, x in sorted(zip(outs, param))]  # sort params and outs equally
                sorted_outs = sorted(outs)
                binned = []  # for the parameters in the individual bins
                for bin_i, bin_up in enumerate(bins):
                    curr = []
                    for outnum, out in enumerate(sorted_outs):
                        if bin_i == 0:  # in first iteration, only upper boundary check
                            if np.log10(out) <= bin_up:
                                curr.append(sorted_params[outnum])
                            else:
                                break  # as we sorted the out list, we can stop once values are too large
                        else:  # from second iteration, check both boundaries
                            if bins[bin_i - 1] < np.log10(out):
                                if np.log10(out) <= bin_up:
                                    curr.append(sorted_params[outnum])
                                else:
                                    break  # as we sorted the out list, we can stop once values are too large
                    binned.append(curr)


            else: #bin size defined by error threshold
                sorted_params = [x for _, x in sorted(zip([x[sortbyout] for x in preds], param))]  # sort params and outs equally
                outs_sort = preds[preds[:,sortbyout].argsort()]
                n_bins = 1
                binned = []
                binned_outs = []
                for i in range(outs_sort.shape[0]):
                    if i == 0: #in first iteration, initialize first bin
                        current_bin = [sorted_params[i]]
                        current_measure = outs_sort[i] # and select fit to serve as current lower boundary
                        current_outs_bin = [outs_sort[i]]
                    elif error_th_calibration.calc_partial_error_Xsets([current_measure], [outs_sort[i]], logabs=log) < subcluster_error_bin: #if error is smaller than threshold, add current fit to bin
                        current_bin.append(sorted_params[i])
                        current_outs_bin.append(outs_sort[i])
                    else: #if not, save bin and open new one
                        binned.append(current_bin)
                        binned_outs.append(current_outs_bin)
                        current_bin = [sorted_params[i]]
                        current_outs_bin = [outs_sort[i]]
                        current_measure = outs_sort[i]
                        n_bins = n_bins+1

                #remove bins with only few elements TODO: good idea???
                rem = []
                for i, bin in enumerate(binned):
                    if len(bin) < 20:
                        rem.append(i)
                binned = [i for j, i in enumerate(binned) if j not in rem]
                binned_outs = [i for j, i in enumerate(binned_outs) if j not in rem]

                if len(rem) > 0:
                    print("Number of bins: " + str(n_bins))
                    print("Removed small bins: " + str(len(rem)))

                if sample_plot: #random plot to visualize subclustering
                    if random.randint(0, 100) < 1:
                        cols = ["r", "b", "g", "y", "c", "m", "orange", "brown"]
                        for i in range(len(binned)):
                            for j in range(len(binned_outs[i])):
                                plt.plot(binned_outs[i][j], [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9], c=cols[i])
                        plt.show()

            bin_mins = []
            bin_maxs = []

            # calculate quantified constraint potential for bins
            for param_bin in binned:
                try:
                    bin_mins.append(np.log10(np.min(param_bin)))
                    bin_maxs.append(np.log10(np.max(param_bin)))
                except:
                    print("Getting min/max from bin failed. Bin size:")
                    print(str(len(param_bin)))
                    bin_mins.append(12345678) #this number will probably never be a parameter (log10!)
                    bin_maxs.append(12345678)
            potential_param_constraint = 0
            for i, lowbin1 in enumerate(bin_mins):
                if lowbin1 == 12345678: #check for empty bins and skip if one occurs
                    continue
                for j, lowbin2 in enumerate(bin_mins): #check for empty bins and skip if one occurs
                    if lowbin2 == 12345678:
                        continue
                    if i != j:
                        potential_param_constraint = potential_param_constraint + abs((bin_mins[i] - bin_mins[j]) * abs(i-j)/n_bins) + abs((bin_maxs[i] - bin_maxs[j]) * abs(i-j)/n_bins)
            potential_constraints.append(potential_param_constraint)

    else: #moving average, std and quartile calculation
        for parnum, param in enumerate(params): #instead of binning, we calculate an error of the outputs to their mean to sort them
            sorted_params = [x for _, x in sorted(zip([x[sortbyout] for x in preds], param))]  # sort params and outs equally
            outs_sort = preds[preds[:, sortbyout].argsort()]
            all_mean = []
            for i in range(outs_sort.shape[1]): #get mean outputs of all fits
                size1 = outs_sort.shape[1]
                if outs_sort.shape[1] == 1: #fix zeros only for single output
                    probs = [i for i, x in enumerate(outs_sort) if x <= 0]
                    if len(probs) > 0:
                        missingparams = [sorted_params[i] for i in probs]
                        outs_sort = np.delete(outs_sort, probs)
                        sorted_params = np.delete(sorted_params, probs)
                size0 = outs_sort.shape[0]
                outs_sort = outs_sort.reshape((size0, size1))
                all_mean.append(10**(np.average(np.log10(outs_sort[:,i]))))
            lower_outs = []
            higher_outs = []
            lower_params = []
            higher_params = []
            for i in range(outs_sort.shape[0]): #split into outputs below and above mean (because error ignores +-)
                if outs_sort[i,sortbyout] < all_mean[sortbyout]:
                    lower_outs.append(outs_sort[i,:])
                    lower_params.append(sorted_params[i])
                else:
                    higher_outs.append(outs_sort[i,:])
                    higher_params.append(sorted_params[i])
            lower_errs = []
            higher_errs = []
            for i in range(len(lower_params)): #obtain errors
                lower_errs.append(error_th_calibration.calc_partial_error_Xsets([np.array(all_mean)], [lower_outs[i]], logabs=log))
            for i in range(len(higher_params)):
                higher_errs.append(error_th_calibration.calc_partial_error_Xsets([np.array(all_mean)], [higher_outs[i]], logabs=log))
            higher_params = [np.log10(x) for _, x in sorted(zip([x for x in higher_errs], higher_params))]  #use errors to sort params and outs
            higher_outs = [x for _, x in sorted(zip([x for x in higher_errs], higher_outs))]
            higher_errs = sorted(higher_errs)
            lower_params = [np.log10(x) for _, x in sorted(zip([x for x in lower_errs], lower_params), reverse=True)]  # use errors to sort params and outs
            lower_outs = [x for _, x in sorted(zip([x for x in lower_errs], lower_outs), reverse=True)]
            lower_errs = [-x for x in sorted(lower_errs, reverse=True)] # error values for lower outs are now negative
            all_errs_sorted = lower_errs + higher_errs
            all_outs_sorted = lower_outs + higher_outs
            all_params_sorted = lower_params + higher_params

            # now determine moving average and std from all params within determined error difference
            moving_std = []
            moving_avg = []
            moving_9quantile = []
            moving_1quantile = []
            current_err = []
            current_outs_sorted = []
            current_fit_i = []
            if len(all_errs_sorted) == 0:
                return 0

            if outs_sort.shape[1] == 1:  # fix zeros only for single output
                for i in range(len(probs)):
                    all_outs_sorted.insert(0, np.array([0]))
                    if log == 0:
                        all_errs_sorted.insert(0, all_mean[0])
                    else:
                        all_errs_sorted.insert(0, np.log10(all_mean).tolist()[0])
                    all_params_sorted.insert(0, np.log10(missingparams[i]))

            for nr, error in enumerate(all_errs_sorted): # go over all fits
                if nr==(len(all_errs_sorted))-1 or nr%continuous_density == 0: # sample density for method via mod
                    current = []
                    current_err.append(all_errs_sorted[nr])
                    current_outs_sorted.append(all_outs_sorted[nr])
                    for nr2, error2 in enumerate(all_errs_sorted): # compare with each fit in ensemble to collect the ones where error falls below specified threshold
                        if outs_sort.shape[1] == 1:
                            if nr == nr2 or all_outs_sorted[nr] == all_outs_sorted[nr2]:
                                current.append(all_params_sorted[nr2])
                            elif abs(error_th_calibration.calc_partial_error_Xsets([all_outs_sorted[nr]], [all_outs_sorted[nr2]], logabs=log)) < continuous_bins:
                                current.append(all_params_sorted[nr2])
                        else:
                            if nr == nr2:
                                current.append(all_params_sorted[nr2])
                            elif abs(error_th_calibration.calc_partial_error_Xsets([all_outs_sorted[nr]], [all_outs_sorted[nr2]], logabs=log)) < continuous_bins:
                                current.append(all_params_sorted[nr2])
                    moving_avg.append(np.average(current)) #from the collected fits, save average, std and 5%,95% quantiles
                    moving_std.append(np.std(current))
                    moving_9quantile.append(np.quantile(current, 0.95))
                    moving_1quantile.append(np.quantile(current, 0.05))
                    current_fit_i.append(nr)

            # now we calculate a constraint-potential
            constraint_potential = 0

            #we obtain min and max for current parameter as baseline
            minpara = np.min(all_params_sorted)
            maxpara = np.max(all_params_sorted)

            # interpolate moving quartiles
            if len(current_fit_i) == 1:
                q1new = moving_1quantile
                q9new = moving_9quantile
            else:
                smooth1 = interp1d(current_fit_i, moving_1quantile, kind='linear')
                #if continuous_density != 1:
                #    xnew = np.arange(0, len(all_outs_sorted), 1)[int(round(continuous_density/2)):][:-int(round(continuous_density/2))]
                #else:
                xnew = np.arange(0, len(all_outs_sorted), 1)
                q1new = smooth1(xnew)

                smooth9 = interp1d(current_fit_i, moving_9quantile, kind='linear')
                q9new = smooth9(xnew)

            #now we sum up the distance between min/max and points on q1/q9 for constraint potential
            for i in range(len(q1new)):
                constraint_potential = constraint_potential + q1new[i] - minpara + maxpara - q9new[i]

            potential_constraints.append(constraint_potential)



            if random.randint(0, 100000) < (sample_plot * 100000):  # make sample plot to visualize method with probability specified

                #for out in all_outs_sorted:  # all fits
                #    plt.plot(out, [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9], c="grey")
                #plt.show()

                # moving average and quartiles with 0.5-output
                #plt.plot([np.log10(x[sortbyout]) for x in current_outs_sorted], moving_avg)
                #plt.plot([np.log10(x[sortbyout]) for x in current_outs_sorted], moving_9quantile, c="r")
                #plt.plot([np.log10(x[sortbyout]) for x in current_outs_sorted], moving_1quantile, c="r")
                #plt.scatter([np.log10(x[sortbyout]) for x in all_outs_sorted], all_params_sorted, s=1)
                #plt.xlabel("Log10 of time at 50% reaction progress")
                #plt.ylabel("Log10 of kinetic parameter")
                #plt.show()
                #plt.close()

                # moving average and quartiles with logabs error to average
                plt.plot([np.min(all_errs_sorted), np.max(all_errs_sorted)], [minpara, minpara], c="y")
                plt.plot([np.min(all_errs_sorted), np.max(all_errs_sorted)], [maxpara, maxpara], c="y")
                plt.plot(current_err, moving_avg)
                plt.plot(current_err, moving_9quantile, c="r")
                plt.plot(current_err, moving_1quantile, c="r")
                plt.scatter(all_errs_sorted, all_params_sorted, s=1)
                if log == 0:
                    plt.xlabel("Abs error of sample with average of all samples")
                else:
                    plt.xlabel("Logabs error of sample with average of all samples")
                plt.ylabel("Log10 of kinetic parameter")
                plt.show()
                plt.close()

                # moving average and quartiles with logabs error to average
                plt.plot([0, len(all_outs_sorted) - 1], [minpara, minpara], c="y")
                plt.plot([0, len(all_outs_sorted) - 1], [maxpara, maxpara], c="y")
                plt.plot(current_fit_i, moving_avg)
                #plt.plot(current_fit_i, moving_9quantile, c="r")
                plt.plot(xnew, q9new, c="r")
                #plt.plot(current_fit_i, moving_1quantile, c="r")
                plt.plot(xnew, q1new, c="r")
                plt.scatter(range(len(all_params_sorted)), all_params_sorted, s=1)
                plt.xlabel("Fits (ordinal, sorted by error towards average of all fits)")
                plt.ylabel("Log10 of kinetic parameter")
                plt.show()
                plt.close()

    return np.average(potential_constraints)


def handle_preds(preds, density, x_interp, labels=[], interpol_first=1, smooth=1, norm_es=1, extrap=1, fitplot=0):
    "Function to obtain all relevant measures from model predictions of ensemble. Includes calculation of actual Ensemble Spread."

    if preds[0].size == 1:
        mean = np.average(preds)
        std = np.std(preds)
        if mean == 0:
            return 0
        if norm_es == 1:
            return ((2*std)/mean) # simple ensemble spread for only one output value
        else:
            return (2*std)

    #interpolate predictions with given "density"
    interps = []
    if extrap==0:
        xnew = np.arange(min(x_interp), max(x_interp), density*max(x_interp))
    else:
        xnew = np.arange(0, 1, density)
    for pred in preds:
        interps.append(interpolate(pred, density, x_interp, extrap=extrap))

    # if labels are given, perform cluster analysis
    if len(labels) != 0:
        cluster1 = []
        cluster2 = []
        for i in range(len(labels)):
            if labels[i] == 0: # distribute interpolations of predictions by labels
                cluster1.append(interps[i])
            else:
                cluster2.append(interps[i])

        #get ensemble mean for each cluster (same size as original interpolation)
        ens_mean1 = get_ensemble_mean(cluster1)
        ens_mean2 = get_ensemble_mean(cluster2)


        ens_mean_diff = []
        for i in range(len(ens_mean1)):
            ens_mean_diff.append(ens_mean1[i] - ens_mean2[i]) #calculate difference at each point of ensemble mean

        #makeplot2(xnew, cluster1, cluster2, ens_mean1, ens_mean2)

        #return the average of the calculated difference
        return np.average(np.absolute(ens_mean_diff))

    ens_mean = get_ensemble_mean(interps)
    ens_mean2 = get_ensemble_mean(preds)

    ens_spread = get_ensemble_spread(interps)
    ens_spread2 = get_ensemble_spread(preds)

    # get top and bottom curve of standard deviation
    ens_spread_curvet = []
    ens_spread_curveb = []

    if interpol_first == 1:
        for i, mean in enumerate(ens_mean):
            ens_spread_curvet.append(ens_spread[i] + mean)
            ens_spread_curveb.append(mean - ens_spread[i])
        if fitplot != 0:
            makeplot(preds, xnew, interps, ens_mean, ens_spread_curvet, ens_spread_curveb, xnew)
    else:
        for i, mean in enumerate(ens_mean2):
            ens_spread_curvet.append(ens_spread2[i] + mean)
            ens_spread_curveb.append(mean - ens_spread2[i])
        if fitplot != 0:
            makeplot(preds, xnew, interps, ens_mean2, ens_spread_curvet, ens_spread_curveb, x_interp)

    # calculate "ensemble spread" by integration of the curves
    if interpol_first == 1:
        spreadval = interp_integration(ens_mean, ens_spread_curvet, ens_spread_curveb, xnew, norm_es=norm_es)
    else:
        spreadval = interp_integration(ens_mean2, ens_spread_curvet, ens_spread_curveb, x_interp, smooth=smooth, norm_es=norm_es)

    return spreadval

def interp_integration(mean, stdtop, stdbot, x, append1=0, smooth=1, norm_es=1):
    "Interpolates mean curve and curves including standard deviation to integrate them and calculate actual Ensemble Spread. smooth=1 is 2nd order spline interpolation, smooth=0 is 1st order spline interpolation."

    # handle different numbers of outputs TODO: pass as argument
    if append1:
        x = np.append(np.array(x),1).reshape((len(x),))
        ymean = np.array(np.append(mean, 0)).reshape((len(mean),))
        ystdtop = np.array(np.append(stdtop, 0)).reshape((len(stdtop),))
        ystdbot = np.array(np.append(stdbot, 0)).reshape((len(stdbot),))
    else:
        ymean = np.array(mean).reshape((len(mean),))
        ystdtop = np.array(stdtop).reshape((len(stdtop),))
        ystdbot = np.array(stdbot).reshape((len(stdbot),))

    if smooth == 1: # 2nd order spline interpolation
        meanf = interp1d(x, ymean, kind='quadratic', fill_value="extrapolate")
        stdtopf = interp1d(x, ystdtop, kind='quadratic', fill_value="extrapolate")
        stdbotf = interp1d(x, ystdbot, kind='quadratic', fill_value="extrapolate")

        mean_surf = integrate.quad(meanf, min(x), max(x))[0]
        stdtop_surf = integrate.quad(stdtopf, min(x), max(x))[0]
        stdbot_surf = integrate.quad(stdbotf, min(x), max(x))[0]

    else: # 1st order spline interpolation
        meanf = interp1d(x, ymean, kind='slinear')
        stdtopf = interp1d(x, ystdtop, kind='slinear')
        stdbotf = interp1d(x, ystdbot, kind='slinear')

        mean_surf = integrate.quad(meanf, min(x), max(x))[0]
        stdtop_surf = integrate.quad(stdtopf, min(x), max(x))[0]
        stdbot_surf = integrate.quad(stdbotf, min(x), max(x))[0]

    if norm_es == 1:
        try:
            return ((stdtop_surf - stdbot_surf)/abs(mean_surf)) #ensemble spread!
        except:
            return 0

    else:
        return (stdtop_surf - stdbot_surf)
def get_ensemble_spread(interps):
    "Helper function to get standard deviation curves relevant for Ensemble Spread."
    stds = []
    for i in range(interps[0].size):
        curr = []
        for j in range(len(interps)):
            curr.append(interps[j][i])
        stds.append(np.std(curr))
    return stds

def get_ensemble_mean(interps):
    "Helper function to get mean curve relevant for Ensemble Spread."
    avgs = []
    for i in range(interps[0].size):
        curr = []
        for j in range(len(interps)):
            curr.append(interps[j][i])
        avgs.append(np.average(curr))
    return avgs

def interpolate(pred, density, x_interp, inv=0, extrap=1):
    "Helper function to perform actual interpolation with scipy-library. Inv inverses the interpolation and is not needed anymore."
    #x = np.array([0.1, 0.5, 0.9, 1])
    #x = np.array([0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])
    x = np.array(x_interp).reshape((len(x_interp,)))
    try:
        y = np.array(pred).reshape((len(x_interp),))
    except:
        y = np.array(np.append(pred, 0))
        y = np.array(y).reshape((len(x_interp),))
    if extrap == 0:
        if inv == 0:
            smooth = interp1d(x, y, kind='quadratic', fill_value="extrapolate")
            xnew = np.arange(min(x), max(x), density*max(x))
            ynew = smooth(xnew)  # use interpolation function returned by `interp1d`
        else:
            smooth = interp1d(y, x, kind='quadratic', fill_value="extrapolate")
            xnew = np.arange(min(y), max(y), density*max(abs(y)))
            ynew = smooth(xnew)
    else:
        if inv == 0:
            smooth = interp1d(x, y, kind='quadratic', fill_value="extrapolate")
            xnew = np.arange(0, 1, density)
            ynew = smooth(xnew)  # use interpolation function returned by `interp1d`
        else:
            smooth = interp1d(y, x, kind='quadratic', fill_value="extrapolate")
            xnew = np.arange(min(y), max(y), density*max(abs(y)))
            ynew = smooth(xnew)
    #plt.plot(xnew, ynew)
    #plt.scatter(x, y, s=20)
    #plt.show()

    if inv == 0:
        return ynew
    else:
        return xnew, ynew

def get_grid(gridsize, gridbounds, loggrid=1):
    "Helper function to get grid of desired size."
    if loggrid == 0:
        Xscale = np.arange(gridbounds[0][0], gridbounds[0][1], (gridbounds[0][1] - gridbounds[0][0])/gridsize[0])
        if len(gridsize) > 1:
            Yscale = np.arange(gridbounds[1][0], gridbounds[1][1], (gridbounds[1][1]-gridbounds[1][0])/gridsize[1])
        if len(gridsize) > 2:
            Zscale = np.arange(gridbounds[2][0], gridbounds[2][1], (gridbounds[2][1]-gridbounds[2][0])/gridsize[2])
            return [Xscale, Yscale, Zscale]
        if len(gridsize) > 1:
            return [Xscale, Yscale]
        return [Xscale]
    else:
        Xscale = np.logspace(gridbounds[0][0], gridbounds[0][1], gridsize[0])
        if len(gridsize) > 1:
            Yscale = np.logspace(gridbounds[1][0], gridbounds[1][1], gridsize[1])
        if len(gridsize) > 2:
            Zscale = np.logspace(gridbounds[2][0], gridbounds[2][1], gridsize[2])
            return [Xscale, Yscale, Zscale]
        if len(gridsize) > 1:
            return [Xscale, Yscale]
        return [Xscale]

def extract_fits(fitfile, n_fits, labels=0):
    "Helper function to extract Fits from provided file."
    allfits = pd.read_csv(fitfile)
    allfits = allfits.dropna()
    allfits = allfits.reset_index(drop=True)
    selectedfits = allfits.iloc[:n_fits, :]
    if labels == 1:
        return selectedfits["label"]
    return selectedfits

def transform_mat_to_csv(filename):
    "Independent function to get KM3-Fits from Matlab file and save as csv."
    if filename.endswith(".mat"):
        # print(os.path.join(directory, filename))
        mat = scipy.io.loadmat(filename)
        df = pd.DataFrame(data=mat["OUT_GOOD"]).iloc[:, :7]

    #all_data = all_data.sort_values([24], axis=0)
    header = ["RS1", "RB1", "dXY", "dYY", "HXY", "deX", "acX"]
    df.to_csv("matlab_fits.csv", header=header, index=False)
    return

def nn_model_pred(inpts, NNmodel, scaler_x, scaler_y, log=0):
    "Helper function to obrain simple NN model prediction for specified inputs. Normal or logarithmic output (log)."
    inpts_log = np.log10(inpts)
    try:
        inpts_t = scaler_x.transform(inpts_log)
    except:
        print("Ensemble Method failed. The restrictions may not allow a valid experiment or the number of remaining fits is 0.")
        import sys
        sys.exit()
    pred = NNmodel.predict(inpts_t)
    pred_t = scaler_y.inverse_transform(pred)
    pred_f = 10**pred_t

    if log==0:
        return pred_f
    else:
        return pred_t

def makeplot(preds, xnew, interps, ens_mean, ens_spread_curvet, ens_spread_curveb, x_interp):
    "Plotting function for visualization during testing. (Not currently used)"
    for i, pred in enumerate(preds):
        plt.plot(interps[i], xnew, c="grey", linewidth=0.05, zorder=0)

    plt.plot(ens_mean, x_interp, c="blue", zorder=1)
    plt.plot(ens_spread_curvet, x_interp, c="red", zorder=1)
    plt.plot(ens_spread_curveb, x_interp, c="red", zorder=1)
    plt.xlabel("Time [s]")
    plt.ylabel("Normalized concentration")

    #if 0: #removed for now, TODO: needs to be updated for new grid boundaries
    #    if (nr == 86 and ni == 50):
    #        plt.scatter([52791.00105, 15891.67482, 2415.583732], [0.1, 0.5, 0.9], marker="o", color="black", zorder=2)
    #    if (nr == 37 and ni == 64):
    #        plt.scatter([62.38715436, 18.78040481, 2.854679627], [0.1, 0.5, 0.9], marker="o", color="black", zorder=2)
    #    if (nr == 46 and ni == 90):
    #        plt.scatter([4.277353791, 1.287611793, 0.195720976], [0.1, 0.5, 0.9], marker="o", color="black", zorder=2)
    #    if (nr == 40 and ni == 72):
    #        plt.scatter([41.71349806, 12.55701414, 1.908704994], [0.1, 0.5, 0.9], marker="o", color="black", zorder=2)
    #    if (nr == 40 and ni == 75):
    #        plt.scatter([24.55174168, 7.39081069, 1.123426088], [0.1, 0.5, 0.9], marker="o", color="black", zorder=2)
    #    if (nr == 40 and ni == 79):
    #        plt.scatter([11.20315814, 3.372486647, 0.512628403], [0.1, 0.5, 0.9], marker="o", color="black", zorder=2)
    plt.savefig("fitplot" + str(random.randint(0,100)) + ".pdf")
    #plt.show()
    plt.close()

    return

def makeplot2(xnew, cluster1, cluster2, ens_mean1, ens_mean2):
    "Another deactivated plotting function for testing."
    for i, intp in enumerate(cluster1):
        plt.plot(intp, xnew, c="blue", linewidth=0.1, zorder=0)
    for i, intp in enumerate(cluster2):
        plt.plot(intp, xnew, c="yellow", linewidth=0.1, zorder=0)
    plt.plot(ens_mean1, xnew, c="green", linewidth=2, zorder=1)
    plt.plot(ens_mean2, xnew, c="red", linewidth=2, zorder=1)

    #if 0: #removed for now, TODO: needs to be updated for new grid boundaries
    #    if (nr == 86 and ni == 50):
    #        plt.scatter([52791.00105, 15891.67482, 2415.583732], [0.1, 0.5, 0.9], marker="o", color="black", zorder=2)
    #    if (nr == 37 and ni == 64):
    #        plt.scatter([62.38715436, 18.78040481, 2.854679627], [0.1, 0.5, 0.9], marker="o", color="black", zorder=2)
    #    if (nr == 46 and ni == 90):
    #        plt.scatter([4.277353791, 1.287611793, 0.195720976], [0.1, 0.5, 0.9], marker="o", color="black", zorder=2)
    #    if (nr == 40 and ni == 72):
    #        plt.scatter([41.71349806, 12.55701414, 1.908704994], [0.1, 0.5, 0.9], marker="o", color="black", zorder=2)
    #    if (nr == 40 and ni == 75):
    #        plt.scatter([24.55174168, 7.39081069, 1.123426088], [0.1, 0.5, 0.9], marker="o", color="black", zorder=2)
    #    if (nr == 40 and ni == 79):
    #        plt.scatter([11.20315814, 3.372486647, 0.512628403], [0.1, 0.5, 0.9], marker="o", color="black", zorder=2)

    plt.show()

    return

