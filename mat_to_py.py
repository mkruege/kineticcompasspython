# DATA TRANSFORMATION FROM MATLAB TO PYTHON
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08

import numpy as np
import pandas as pd
from scipy import io
import os
from pickle import dump, load

def transform_grid_mat_to_pickle(path, outname, outputs=9, inpt_paramsets="km3_fits_no_preds.pickle"):
    #get individual files
    onlyfiles = next(os.walk(path))[2]  # dir is your directory path as string

    #collect data from files
    all_data = []
    for filenum, filename in enumerate(onlyfiles):
        if filename.endswith(".mat"):
            mat = io.loadmat(path + "/" + filename)
            data = mat["GRID"]
            all_data.append(data)

    #transform data to grid structure
    grid_arr = np.zeros((len(all_data[0]),len(all_data[0][0])), dtype=object)
    for parnum, param in enumerate(all_data):
        for colnum, col in enumerate(param):
            for valnum, val in enumerate(col):
                if parnum == 0:
                    grid_arr[colnum][valnum] = [val.tolist()]
                else:
                    grid_arr[colnum][valnum].append(val.tolist())

    #change data types and structure
    all_preds = []
    for col in grid_arr: #100
        for out in col: #100
            count = -1
            outarray = np.zeros((len(all_data), outputs))
            for paramset in out: #500
                count = count+1
                try:
                    for valnum, val in enumerate(paramset): #9
                        outarray[count][valnum] = val
                except:
                    print(paramset)
            all_preds.append(outarray)

    #get paramsets data and save both
    [all_preds_params, notrelevant] = load(open(inpt_paramsets, "rb"))
    dump([all_preds_params, all_preds], open(outname + ".pickle", "wb"))

    return


def transform_inpt_mat_to_csv(filename):
    if filename.endswith(".mat"):
        # print(os.path.join(directory, filename))
        mat = io.loadmat(filename)
        df = pd.DataFrame(data=mat["OUT_GOOD"]).iloc[:, :7]

    #all_data = all_data.sort_values([24], axis=0)
    header = ["RS1", "RB1", "dXY", "dYY", "HXY", "deX", "acX"]
    df.to_csv("km3_fits.csv", header=header, index=False)

    return