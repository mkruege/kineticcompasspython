# ALTERNATIVE APPLICATION OF THE KINETIC COMPASS FRAMEWORK FOR EXPERIMENTAL CONDITIONS SENSITIVITY
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08


import pandas as pd
import numpy as np
import pickle
from tensorflow import keras
import error_th_calibration
import plotting

def perform_error_meth(fitfile, n_fits, gridsize, model, scaler, savedir = "", labelsf=[], fulldata=[], use_pickle=[]):
    "Function that performs the Error Method instead of the Ensemble Method, mapping the potential errors of experiments based on the selection of environmental parameters as suggested by model. Indicated by errors towards neighboring grid cells in specific areas of the environmental parameter grid."

    # LOAD NN FORWARD MODEL (3 outputs)
    NNmodel = keras.models.load_model(model)

    # LOAD x- and y-scaler associated to this model
    [scaler_x, scaler_y] = pickle.load(open(scaler, "rb"))

    #extract n_fits parameter sets from fitfile
    if len(fulldata) == 0:
        fits = extract_fits(fitfile, n_fits)
    else:
        fits = fulldata[0][:n_fits]


    #get grid in log-space
    [rad, io3] = get_grid(gridsize)

    #rad = [1.00E-03, 2.00E-05, 0.00004, 2.50E-05, 2.50E-05, 2.50E-05]
    #io3 = [9970000000000, 70000000000000, 2.50E+15, 2.00E+14, 3.25E+14, 5.51E+14]

    # get labels
    if len(fulldata) == 0:
        if len(labelsf) != 0:
            labels = extract_fits(labelsf[0], n_fits, labels=1)
    else:
        labels = fulldata[1][:n_fits]

    # loop over grid and save ensemble data
    step = 0
    inpts1 = []
    inpts2 = []
    heatmap = np.zeros((gridsize, gridsize))
    dist = np.zeros((gridsize, gridsize))

    if len(use_pickle) == 0: #obtain predictions with model
        all_preds = []
        for nr, r in enumerate(rad):
            sub_preds = []
            for ni, i in enumerate(io3):
                inpts1.append(r)
                inpts2.append(i)
                # progress
                step = step + 1
                if step % 100 == 0:
                    print("Step " + str(step) + " of " + str(len(rad) * len(io3)))

                # get predictions for current ensemble
                fits["rad"] = [r for x in range(n_fits)]
                fits["X0"] = [i for x in range(n_fits)]
                fits["Y0"] = [1.89 * 10 ** 21 for x in range(n_fits)]

                # use NN to make predictions and save all parameters and predictions for pickle
                preds = nn_model_pred(fits, NNmodel, scaler_x, scaler_y)
                sub_preds.append(preds)
            all_preds.append(sub_preds)

    else: #read predictions from pickled grid
        [all_preds_params, all_preds_unsort] = pickle.load(open(use_pickle[0], "rb"))
        all_preds = []
        curr = 0
        for i in range(int(np.sqrt(len(all_preds_unsort)))):
            sublist = []
            for j in range(int(np.sqrt(len(all_preds_unsort)))):
                sublist.append(all_preds_unsort[curr])
                curr = curr + 1
            all_preds.append(sublist)

    # we calculate errors to each of the neighboring grid cells to determine potential error with even uncertainty of env. parameter selection
    errors = []
    for i in range(gridsize):
        sub_errors = []
        for j in range(gridsize):
            current_out = all_preds[i][j]
            compare = []
            if i != 0:
                compare.append(all_preds[i-1][j])
            if i != (gridsize-1):
                compare.append(all_preds[i+1][j])
            if j != 0:
                compare.append(all_preds[i][j-1])
            if j != (gridsize-1):
                compare.append(all_preds[i][j+1])

            subsub_errors = []
            for k in range(n_fits):
                subsubsub_errors = []
                for l, noth in enumerate(compare):
                    subsubsub_errors.append(error_th_calibration.calc_partial_error_Xsets([current_out[k]], [compare[l][k]]))
                subsub_errors.append(np.average(subsubsub_errors))
            sub_errors.append(np.average(subsub_errors))
            heatmap[i][j] = np.average(subsub_errors)
        errors.append(sub_errors)

    returnmatrix = pd.DataFrame(data=heatmap, index=rad, columns=io3)
    returnmatrix.to_csv(savedir + "error_map_matrix.csv")

    #returndf = pd.DataFrame()
    #returndf["rad"] = inpts1
    #returndf["X0"] = inpts2
    #returndf["error_map"] = errors

    #returndf = returndf.sort_values("error_map", ascending=False)
    #returndf.to_csv(savedir + "error_map.csv")

    plotting.make_contour_plot(savedir + "error_map_matrix.csv", xt=[-6, -5.5, -5, -4.5, -4, -3.5, -3, -2.5, -2],
                               yt=[10, 11, 12, 13, 14, 15, 16], savedir=savedir,
                               gridsize=gridsize)
    #plotting.make_contour_plot(savedir + "error_map_cluster_mean_diff_matrix.csv",
    #                           xt=[-6, -5.5, -5, -4.5, -4, -3.5, -3, -2.5, -2], yt=[10, 11, 12, 13, 14, 15, 16],
    #                           savedir=savedir, gridsize=gridsize)
    #plotting.make_contour_plot(savedir + "error_map_cluster_mean_diff_matrix.csv", log=1,
    #                           xt=[-6, -5.5, -5, -4.5, -4, -3.5, -3, -2.5, -2], yt=[10, 11, 12, 13, 14, 15, 16],
    #                           savedir=savedir, gridsize=gridsize)

    return


def get_grid(gridsize):
    "Helper function to get grid of desired size (boundaries are hard-coded)."

    #original:
    #Xscale = np.logspace(-5.60206, -3, gridsize)
    #Zscale = np.logspace(11, 15, gridsize)

    #extended:
    #Xscale = np.logspace(-6, -2.5, gridsize)
    #Zscale = np.logspace(10, 16, gridsize)

    #extended2:
    Xscale = np.logspace(-6, -2, gridsize)
    Zscale = np.logspace(10, 16, gridsize)

    # for realistic experiments:
    #Xscale = np.logspace(-5.301, -2, gridsize)
    #Zscale = np.logspace(12, 16, gridsize)

    #[X, Z] = np.meshgrid(Xscale, Zscale)

    return [Xscale, Zscale]


def extract_fits(fitfile, n_fits, labels=0):
    "Helper function to extract fits from file."

    allfits = pd.read_csv(fitfile)
    allfits = allfits.dropna()
    allfits = allfits.reset_index(drop=True)
    selectedfits = allfits.iloc[:n_fits, :]
    if labels == 1:
        return selectedfits["label"]

    return selectedfits

def nn_model_pred(inpts, NNmodel, scaler_x, scaler_y, log=0):
    "Helper function to obtain simple NN surrogate model prediction."

    inpts_log = np.log10(inpts)
    try:
        inpts_t = scaler_x.transform(inpts_log)
    except:
        print("Ensemble Method failed. The restrictions may not allow a valid experiment or the number of remaining fits is 0.")
        import sys
        sys.exit()
    pred = NNmodel.predict(inpts_t)
    pred_t = scaler_y.inverse_transform(pred)
    pred_f = 10**pred_t

    if log==0:
        return pred_f
    else:
        return pred_t