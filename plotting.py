# PLOTTING TOOLS FOR THE KINETIC COMPASS
# Matteo Kr�ger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/08/08

import copy
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import pickle
from matplotlib.ticker import MaxNLocator
import matplotlib.colors as mcolors
from matplotlib.colors import LinearSegmentedColormap


def make_only_grid_contourplot():
    grid_list_x = []
    grid_list_y = []
    for i in range(100):
        for j in range(100):
            grid_list_x.append(i)
            grid_list_y.append(j)
    plt.scatter(grid_list_x, grid_list_y, s=0.2, c="black")
    plt.savefig("gridpoints.pdf")
    plt.show()
    return


def make_hillplot(es_file):
    df = pd.read_csv(es_file, header=[0], index_col=[0])
    X = [float(x) for x in list(df.columns.values)]
    Y = list(df.index.values)
    Z = df.to_numpy()
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.contour3D(Y, X, Z.transpose(), 5000, cmap='binary')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])
    ax.w_xscale.set_scale('log')
    ax.w_yscale.set_scale('log')
    ax.set_zlim(-2, 5)
    fig.savefig("3d_surface.pdf")
    fig.show()
    return

def make_fit_plots(exppickle, cmprdata):
    [cmprdata_out_d, cmprdata_out_t] = cmprdata
    curr_preds = pickle.load(open(exppickle, "rb"))
    for i in range(len(curr_preds[0])): #iterate over experiments
        plt.figure(figsize=(6, 6), dpi=100)
        plt.gca().xaxis.set_major_locator(MaxNLocator(nbins=5))
        for j in range(len(curr_preds)): #iterate over fits
            plt.plot(curr_preds[j][i]+[0], [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1], c='grey', zorder=0, linewidth=0.2)
        plt.scatter(cmprdata_out_t[i], cmprdata_out_d[i], zorder=1, s=40, c='black')
        plt.yticks([0, 0.2, 0.4, 0.6, 0.8, 1], fontsize=16)
        plt.xticks(fontsize=16)
        plt.xlabel("Time [s]", fontsize=18)
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        plt.ylabel("Normalized concentration", fontsize=18)
        plt.savefig("fitplot_" + str(i) + ".pdf")
        plt.show()
    return


def make_contour_plot(data, cmprdata, vmin=0, vmax=0, log=0, xt=[], yt=[], exp=1, add_exp=[], savedir="", run=0, colors=0, lockgrid=[], units=1):
    "Helper function to make contour plots for Ensemble Spread or Potential Constraint or Error Method."
    gridsize=100
    df = pd.read_csv(data, index_col=0)

    #plt.close()
    #plt.rcParams['xtick.labelsize'] = 12
    #plt.rcParams['ytick.labelsize'] = 12

    X0 = []
    X0.append([float(list(df.columns.values)[x]) for x in range(len(list(df.columns.values)))])
    X0 = X0[0]
    r = df.index.tolist()

    #outdated grid boundaries:
    #Xscale = np.logspace(-6, -2.5, 10000)
    #Zscale = np.logspace(10, 16, 10000)

    #latest:
    Xscale = np.logspace(-6, -2, 99*gridsize)
    Zscale = np.logspace(10, 16, 99*gridsize)

    cmprdata_in = copy.deepcopy(cmprdata[0])

    if len(add_exp) != 0:
        for aexp in add_exp:
            cmprdata_in.append(aexp + [1.89E+21])

    cmprd_r = [cmprdata_in[x][0] for x in range(len(cmprdata_in))]
    cmprd_x = [cmprdata_in[x][1] for x in range(len(cmprdata_in))]

    exp_r = []
    exp_x = []
    for i in range(len(cmprdata_in)):
        X = Xscale.tolist()
        Z = Zscale.tolist()
        X.append(cmprd_r[i])
        X.sort()
        exp_r.append(X.index(cmprd_r[i])/100)
        Z.append(cmprd_x[i])
        Z.sort()
        exp_x.append(Z.index(cmprd_x[i])/100)

    #debugging...
    #print(df.shape)
    #[print(x*(gridsize/(len(xt)-1))) for x in range(len(xt))]
    #print(xt)
    #[print(x*(gridsize/(len(yt)-1))) for x in range(len(yt))]
    #print(yt)
    #print(exp_r)
    #print(exp_x)
    #print(savedir)
    #print(data)

    if colors == 0:
        colmap = 'coolwarm'
    elif colors == 1:
        colmap = mcolors.LinearSegmentedColormap.from_list("", ["blue", "green", "yellow", "orange", "red"])(np.linspace(0.2, 1, 256))
    elif colors == 2:
        # Define the rainbow colormap
        rainbow_colors = [(0, 0, 1), (0, 1, 1), (0, 1, 0), (1, 1, 0), (1, 0, 0)]
        rainbow_cmap = LinearSegmentedColormap.from_list('rainbow', rainbow_colors)
        # Create the truncated colormap
        new_colors = rainbow_cmap(np.linspace(0.1, 0.9, 256))
        colmap = LinearSegmentedColormap.from_list('truncated_rainbow', new_colors)
    elif colors == 3:
        # Define the rainbow colormap
        rainbow_colors = [(0, 0, 1), (0, 1, 1), (0, 1, 0), (1, 1, 0), (1, 0, 0)]
        rainbow_cmap = LinearSegmentedColormap.from_list('turbo', rainbow_colors)
        # Create the truncated colormap
        new_colors = rainbow_cmap(np.linspace(0.05, 0.95, 256))
        colmap = LinearSegmentedColormap.from_list('truncated_turbo', new_colors)
    elif colors == 4:
        colmap = 'cool'
    elif colors == 5:
        cmap = plt.cm.get_cmap('PuOr_r')
        colmap = LinearSegmentedColormap.from_list('truncated_PuOr_r', cmap(np.linspace(0.2, 1, 256)))
    else:
        colmap = 'coolwarm'

    if vmin != 0 or vmax != 0:
        if units == 0:
            levels = np.linspace(vmin, vmax, 40)
            plt.contourf(df, levels=levels, cmap=colmap, fontsize=12)
            plt.contourf(df, levels=levels, cmap=colmap, fontsize=12)
            if len(xt)!=0:
                plt.yticks([x*((gridsize-1)/(len(xt)-1)) for x in range(len(xt))], xt, fontsize=12)
            if len(yt)!=0:
                plt.xticks([x*((gridsize-1)/(len(yt)-1)) for x in range(len(yt))], yt, fontsize=12)
            plt.xlabel("log10 of ozone concentration (cm$^{-3}$)", fontsize=14)
            plt.ylabel("log10 of particle radius (cm)", fontsize=14)
        else:
            levels = np.linspace(vmin, vmax, 40)
            plt.contourf(df, levels=levels, cmap=colmap, fontsize=12)
            plt.contourf(df, levels=levels, cmap=colmap, fontsize=12)
            X0_ppb = np.log10([x/2.46e10 for x in X0])
            round_numbers = [0, 1, 2, 3, 4, 5]
            positions = np.interp(round_numbers, X0_ppb, np.arange(len(X0_ppb)))
            plt.xticks(positions, round_numbers, fontsize=14)
            #plt.xticks([x * ((gridsize - 1) / (len(xt_ppb) - 1)) for x in range(len(xt_ppb))], xt_ppb, fontsize=12)
            plt.xlabel("log10 of ozone concentration (ppb)", fontsize=14)
            yt_nm = np.log10([10**x * 1e7 for x in xt])
            plt.yticks([y * ((gridsize - 1) / (len(yt_nm) - 1)) for y in range(len(yt_nm))], yt_nm, fontsize=12)
            plt.ylabel("log10 of particle radius (nm)", fontsize=14)
    elif log == 1:
        df = np.log10(df.abs())
        plt.contourf(df, levels=40, cmap=colmap)
        plt.contourf(df, levels=40, cmap=colmap)
        if len(xt)!=0:
            plt.yticks([x*((gridsize-1)/(len(xt)-1)) for x in range(len(xt))], xt)
        if len(yt)!=0:
            plt.xticks([x*((gridsize-1)/(len(yt)-1)) for x in range(len(yt))], yt)
        plt.xlabel("log10 of ozone concentration (cm$^{-3}$)", fontsize=14)
        plt.ylabel("log10 of particle radius (cm)", fontsize=14)
    else:
        plt.contourf(df, levels=40, cmap=colmap)
        plt.contourf(df, levels=40, cmap=colmap)
        if len(xt)!=0:
            plt.yticks([x*((gridsize-1)/(len(xt)-1)) for x in range(len(xt))], xt)
        if len(yt)!=0:
            plt.xticks([x*((gridsize-1)/(len(yt)-1)) for x in range(len(yt))], yt)
        plt.xlabel("log10 of ozone concentration (cm$^{-3}$)", fontsize=14)
        plt.ylabel("log10 of particle radius (cm)", fontsize=14)
    if vmin == 0 and vmax == 2.5:
        plt.colorbar(ticks=[0, 0.5, 1, 1.5, 2, 2.5])
    else:
        plt.colorbar()

    if len(lockgrid) != 0:
        lockbounds = pickle.load(open(lockgrid[0], "rb"))
        plt.contour(lockbounds, levels=[0.5], colors='b', linewidths=5)

    if exp == 1:
        if len(add_exp) == 0:
            plt.scatter(exp_x, exp_r, s=10, c="black")
        else:
            plt.scatter(exp_x[:7], exp_r[:7], s=10, c="black")
            plt.scatter(exp_x[7:], exp_r[7:], s=10, c="green")
    if log != 1:
        if run == 0:
            try:
                plt.savefig(savedir + data[:-4] + ".pdf")
            except:
                plt.savefig(data[:-4] + "_iter_" + str(run) + ".pdf")
        else:
            plt.savefig(data[:-4] + "_iter_" + str(run) + ".pdf")
    else:
        if run == 0:
            try:
                plt.savefig(savedir + data[:-4] + "_log.pdf")
            except:
                plt.savefig(data[:-4] + "_log_iter_" + str(run) + ".pdf")
        else:
            plt.savefig(data[:-4] + "_log_iter_" + str(run) + ".pdf")
    plt.show()
    plt.close()
    return